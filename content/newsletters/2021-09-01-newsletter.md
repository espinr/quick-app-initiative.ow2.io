---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: false
title: Quick App Initiative Newsletter 
subtitle: September 2021
date: 2021-09-01
tags: ["newsletter"]
bigimg: [{src: "/img/bulletin.jpg", desc: "Bulletin board"}]
comments: false
---

Highlights of the Quick App Initiative, including:
- [Open Source Experience Paris, 9-10 November](#open-source-experience-paris-9-10-november)
- [Task Forces and discussion groups](#task-forces-and-discussion-groups)
- [A big welcome to Zachary Powell](#a-big-welcome-to-zachary-powell)
- [Quick Apps strategy workshop (28 July)](#quick-apps-strategy-workshop-28-july)
- [Pilot application for Lutece](#pilot-application-for-lutece)
- [Promotion](#promotion)
- [Housekeeping](#housekeeping)

<!--more-->

### Open Source Experience Paris, 9-10 November

Huawei is gold sponsor of this great event, and will dedicate the majority of its booth to the initiative.
- QAI participants can be part of this:
- Use the booth to talk about your activities and your hopes for the initiative, and even demo your quick apps.
- Be part of a 45 minute workshop (please share your ideas!).
- We propose to display logos of participants on the booth and flyer.
- Design work will start soon and must be finalised early October.

### Task Forces and discussion groups

3 task forces are in the process of being launched:
- Gaming (FRVR + Famobi)
- Education (Santillana, CTIC, Alliance Tech).
- Sustainability (Alliance Tech)
- There is also an informal discussion group about outreach to Russia (Kester Bishop and Adrien Henni).
- Each of the above will provide insight and news during the next steering committee.
- In the meantime, if any of these groups interests you, please let us know. The more people we have in each the better they will be.
 
### A big welcome to Zachary Powell
- Zach will be helping the initiative as a Developer Advocate.
- He’s of course a very friendly guy, and has huge experience in Android programming; we’re super lucky to have him participating with us.
- If you would like to say hi and learn more, drop him a message [on twitter](https://twitter.com/devwithzachary) or [LinkedIn](https://www.linkedin.com/in/zachary-mg-powell/) 
 
### Quick Apps strategy workshop (28 July)

Two-hour workshop to analyse challenges and opportunities of quick apps. 40+ participants in total.
- Target: defining actions (quick wins) for spreading quick apps in the European market.
- Highlighted the lack of support in OS/devices and platforms, and lack of developer support outside China.
- Top strengths: 
  - Quick app has unique features to maximize user engagement and retention from a marketing perspective, and 
  - Quick apps are the tool to build and deliver experiences.
- As an outcome, the workshop generated a document with a wide list of recommendations and insights from different angles (technology, business development and community).
- Soon, we will organize a similar ad hoc workshop on quick app monetization, including advertising and payments.
 
### Pilot application for Lutece

[Lutece](https://lutece.paris.fr/lutece/) is an hugely successful open source citizen services platform managed by the City of Paris, and published through OW2.
- It is used not just by City of Paris, but also other cities like Lyon, Baltimore, Budapest, and some projects in Africa.
- The Lutece team have really seen the potential for quick apps, and have agreed to work with the initiative to create a pilot app.
- If you are interested in being part of the development effort for this pilot, please contact me and I will invite you into the next discussion meeting when will look closer at how to organise the pilot.
 
### Promotion

We’ll be speaking about QAI to Systematic’s Hub Open Source later in September.
- [Systematic](https://systematic-paris-region.org/) is a regional competitivity cluster and European Deep Tech Pole based in Paris. It plays a significant role in advancing various ICT domains, nurturing SMEs, and creating a melting pot of innovative SMEs, industry and academia.  
- Its Hub Open Source has a long and important history promoting open source, not least it is responsible for the OSXP event.
- Let’s work together to grow our Quick App Initiative community.
- Reach out to organisations and people who you think could be interested; invite them in, share the relevant links. Myself, Martin and Zach can also talk with them.
- Why not share some love (not in that way) on social media, LinkedIn,...?
 
### Housekeeping

Have you created your OW2 user account and connected to GitLab and chat?

- [OW2 user account](https://www.ow2.org/view/services/registration)
- [QAI chat](https://rocketchat.ow2.org/group/QAI-Town-Square)
- [QAI GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative)
  - GitLab is being updated with lots of “ToDo” (or issues in GitLab language). Make sure to check it out and see where you’d like to help.
- [Portal content](https://quick-app-initiative.ow2.io/)
- Feel free to write a blog article for the portal, or propose other content that you feel would benefit everyone.
 
