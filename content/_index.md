**Quick apps** offer a future facing alternative to the current mobile app dynamic. They provide a breath of fresh air to developers, publishers, businesses and users. At the same time, they also allow us to rethink the way in which mobile apps can better enhance the progressive shift towards a digital everything society. In fact, quick apps offer a **[multitude of benefits](editorials/2021-09-15-quick-apps-speedy-services-less-coding-effort/)**.

Quick apps are an implementation of emerging [W3C MiniApp standards](https://www.w3.org/2021/miniapps/) that provide a framework for mobile applications development based on widely known front-end web technologies (JavaScript, CSS ...). Learn more in our [Primer](https://quick-app-initiative.ow2.io/docs/Quick_App_Primer.pdf), the [White Paper](/page/whitepaper/), or jump directly into the [Quick Starting Guide](/developers).

## The initiative

The [OW2 Quick App Initiative](https://www.ow2.org/view/QuickApp/) (or "QAI" for short) was launched in June 2021 to encourage a _healthy_ and _vibrant_ quick app ecosystem in **Europe and beyond**. It's a **multi-party** forum that embraces the ideals of **open source** (transparency, meritocracy and respectful global collaboration) to explore use cases where quick apps have the potential to *significantly benefit* development efficiency, user satisfaction and business growth. ... **[read more](page/about/#some-context)**

- [Join QAI](https://www.ow2.org/view/QuickApp/Participants_Form)
- [Contribute ](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative)
- [Keep in touch](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)
