---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: Memorandum on Quick Apps and Open Source
subtitle: Olisto shares the vision
date: 2021-10-25
tags: [Olisto, OpEd]
author: Arjen Noorbergen
bigimg: [{src: "/img/posts/2021/olisto.gif", desc: "Olisto"}]
comments: true
draft: true
---

The rationale behind quick apps has been well argued by Christian Paterson in his article "10 reasons for quick apps" [1]. The world is increasingly getting digital. More and more services are being digitized, products and devices are getting connected to the Internet, and modern challenges for our society such as the COVID pandemic demonstrate what the power of digitization can do to address them.

**At Olisto, we share the vision on the opportunities of quick apps.**

<!--more-->

At Olisto, we have built a platform with which users may configure rules and integrations to have products and services work together, bringing small “nuggets” of functionality for the end-user. We are working with large brands, who concur with the idea that small –yet valuable– pieces of functionality added to their products, but do not justify building, publishing and maintaining yet another app in multiple different app stores. Or even another release of their monolithic "Flagship" super app.

---

{{< rawhtml >}}
<div style="padding-left: 5rem; font-style: italic;">
Consider the following case:
<ul>
<li>A telecoms operator might offer parental control features on their TV setup box allowing it to be configured with a PIN code for certain channels so that younger kids cannot see adult content (for instance).</li>
<li>A Wi-Fi router might offer features that allow certain MAC addresses to be blocked from access to the internet, permanently or according to a time schedule, configured through a web-interface.</li>
<li>Wouldn’t it be useful for parents to determine when a game console has internet access? For example, only after a certain time.</li></ul>

<p>Consumers with kids hardly use these features (when available) as they are too complex to manage (and maybe too easy to circumvent), yet when presented with these potential capabilities, consumers certainly voice an interest in them.</p>

<p>The problem is that building a separate app to configure parental controls is a bridge too far for the average telco. It’s cumbersome and costly and will still have a barrier to use: customers need to be pointed to the app and they need to install it.</p>

<p>Should “parental control” features be added to a "Flagship" app, these features need to then compete on the roadmap of such a “do everything” app, where discussions take place on priorities. Features for the Flagship app tend to be chosen to address the largest part of user base, and not a part of the user base with particular needs. Otherwise, the Flagship app would be bloated with features, and become unusable for just about everyone.</p>

<p>At Olisto, we have recognized the case for a middle-ground solution between not doing anything at all on the one hand, and app development on the other hand.</p>
</div>
{{< /rawhtml >}}

---

At Olisto, we can provide small pieces of functionality, addressed with a link or a QR code for user-interfacing, and execution of functionality that runs in the cloud and works to the benefit of <INS>both</INS> end-user and telco (in this case).

So, we definitely concur with the thoughts behind quick apps, although we have a cloud-based approach.

We wholeheartedly support the development of quick apps for reasons mentioned above. Quick apps could provide the configuration user-interface to the back-end functionality and integrations that we provide.

### How about Open Source?

We believe it is crucial that the open source community becomes active in the development of quick apps, whether it's about the engine running on compute resources, driving standards, developing supporting back-end technologies and APIs, or about the creation -and publication- (CMS-like) of tooling to better enable businesses to benefit from quick apps.

In order to drive this whole new paradigm, it is a matter of **all hands on deck**. Across various parts of the industry, people should become active. Whether from research and standardization (W3C), through developers crafting code, to digital agencies spreading the gospel and getting businesses on-board. Non-profit organizations, businesses and individuals will all need to get active.

By making certain technologies open source (such as specifications, APIs, perhaps a reference quick app engine, supporting back-end technology, content tooling), cooperation across many disciplines may be obtained and _sincere_ cooperation (which will be greatly needed) will flourish.

Inversely, a <INS>non</INS>-open source approach will lead to discussions on Intellectual Property, patents and licensing, and will hinder the broad adoption of quick apps across the various disciplines required to participate to a thriving, healthy ecosystem. If we look at an industry such as 5G mobile communication, only a handful of organizations or companies can _truly_ contribute to Standards and implementation. They know where to find one another in Standardization bodies. IP, inventions and patents are attributed to individual companies and traded across industry members. This does not encourage broad participation.

Open source can help address the current hegemony of US big tech corporations that often try to create and maintain captive ecosystems. It can be leveraged by the many as a means to create open alternatives to such captive ecosystems.

Take Apple for instance: whilst they manufacture great phones, they _allegedly_ extort app makers by overly controlling their ability to communicate with and _know_ their users, and by charging a _defacto_ 30% share of revenue (yes, there are exceptions). This "tax" for providing the Apple services to app makers is under scrutiny of regulators world-wide.

Quick apps allow the possibility of organizations to use _alternative channels_ to reach their customers outside the currently captive app stores. Such alternatives should encourage gatekeepers to consider app distribution and payment services at more competitive and market-based pricing.

By gathering an open source community behind quick apps, we can tap from an army of developers whom will then not be fearful they would contribute to creating yet another tech "Moloch".

### Business models

So, is there a business in open source?

Many companies have proven that open source is a sound and solid basis for building great businesses. May I refer to Gitlab? Or Elastic Search? Both companies with a multi billion-dollar valuation, founded on open source.

With a new quick apps industry to be developed, there's lots of open and closed source business to be made in various areas, such as:
- Back-end services (persistence, messaging)
- Cloud services
- Integration
- Creative tooling
- CMS-like tooling
- Integration services
- Payment services
- Identity management services
- Advertising services
- Design services (digital agencies)
- Consulting 

And it is this versatile collection of areas and licensing regimes that is both necessary for ecosystem success, and will benefit from a new era of quick apps.

**Maybe the best example open source power for ecosystem generation is the internet:**

It is very hard to see how the internet, as it is today, would have come to exist if the hyperlink and http-protocol had been commercial products, for which licenses would have had to been obtained, or a 30% fee been payable to a company or consortium for every sale conducted via a link. Open source initiatives have created the internet and should now be stimulated to save the internet from the Big Tech hegemony.

### Olisto’s position

As Olisto, we develop lots of code, none of which we have released as open source ... <INS>Yet</INS>.

Large corporations (namely telecom operators and security companies) have voiced concerns about doing business with us:
- What happens if we are acquired by a competitor of theirs, or we go bankrupt, or we suddenly increase our tariffs by a huge amount?
- Is our code any "good"? Is it scalable? Are there any wide-open security holes?
- If they want to run it on-premises, can we show that there isn't any spyware or backdoors in our software?
- And equally important, aren’t we using 3rd party code in our software for which license fees would be payable to another party and for which they (our customers) might then be liable?
... and so on and so forth.

To counter this, we make available to these corporations our code at no additional cost. And in all cases this has been satisfactory.

Although we have not (yet) released code publicly as open source, this is clearly in the top of our minds, as we believe we can build a sustainable business model by providing services rather than lines-of-code. Also, it is in our interest to encourage others to contribute to our thoughts and code base. We do realise however that running an open source project comes with responsibilities and is by no means an easy ride.

So, we believe that –eventually– we will make a public release of our code, for the reasons provided above.

_Is open source a pre-condition for our participation in the [OW2 Quick App Initiative](https://quick-app-initiative.ow2.io/)?_

It is <INS>definitely preferred</INS> as we believe **it is sensible to use open source as a means to deliver QAI goals**. If on the other hand, certain parties require that they maintain closed some or all of their relevant intellectual property, being part of QAI would only make sense if expected achievements could still be delivered in a timely and efficient manner. Open source is a _means_ rather than an _end_.

For the reasons outlined above, I believe that open source can be a driver of success within Europe. Open source provides a mantra of “nothing to hide” and is a big benefit for players in Europe. Both governments and corporations/enterprises increasingly put big US and Chinese companies under scrutiny and regulatory restrictions. Open source will definitely ease adoption of this new technology in Europe.

### Making quick apps a success

In order to make quick apps a success, various sectors of the digital industry will need to "play along" in my opinion. Open source provides the ability for organisations to easily and truly cooperate in a quest to build ecosystems around quick apps. The technological building blocks must be there, but eventually digital agencies will need to products and services. We should have an eye for all parties that need to play along, as it is the chain of companies that will make quick apps a success.

Final thoughts...

It is going to be absolutely crucial that Android and iOS devices support quick apps. The invested entities behind these market dominating ecosystems may try to block quick app engines from running on their respective platforms. We should be aware of this –and address this- very early on, but if we set out the necessary elements there's no reason why they can't also play ball.

Initially it is thinkable to launch quick apps on a limited number of devices first:
- With only a portion of market share (see also [Beyond addressable market](https://quick-app-initiative.ow2.io/editorials/2021-09-10-building-european-ecosystem/)), or
- With only certain classes of quick apps
  - Those that don't require phone/HW functions,
  - Those that provide the front-end to cloud based functionalities (glorified PWAs).

In the medium-to-long term, participation of Google (Android) and Apple (iOS) will be crucial to ecosystem success, and open source should be seen as a significant (if not guaranteed) enabler of this.

And when turning to regulators in the EU, having a clean “open source” conscience, will also be a benefit.

In short, here at Olisto we share the vision on the opportunities of quick apps. We believe in the importance of bringing together organisations from across the spectrum when looking to create a successful new ecosystem. And, we believe that open source will facilitate this for the benefit of all.

---

SOURCES:

   [1] [https://quick-app-initiative.ow2.io/editorials/2021-09-10-quickapps-to-the-rescue/](https://quick-app-initiative.ow2.io/editorials/2021-09-10-quickapps-to-the-rescue/)

---

Feel free to sign-up to the OW2 Quick App Initiative **[mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)** today.
