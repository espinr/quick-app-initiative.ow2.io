---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: The Instant Future
subtitle:
date: 2021-11-04
tags: ["FRVR","OpEd"]
author: Cláudio Jahate
bigimg: [{src: "/img/posts/2021/smart-ge0a9d5131_1920.jpg", desc: "Image by Tumisu from Pixabay"}]
comments: true
draft: true
---

The other day I stopped to think about how quickly we can get, or do, almost everything nowadays. We have a random question, we search for the answer on our phones. We want to see our family on the other side of the world, we can simply video call them. We want the world to know that we’ve done something, we make a post on social media. We can order food from one of our favorite restaurants in town and get it still warm in a matter of minutes. We can even schedule a date with a complete stranger in a matter of minutes - how crazy is that?

The society that we are living in is one where the barriers of speed are being challenged everyday by technology, and this will only keep increasing in the upcoming years.

<!--more-->

**The future is instant, and there’s no way of denying that**

This shift in the way human beings experience speed, makes us prioritize actions that require the least amount of effort, but can still have the desired outcome that we are looking for. Time is money, and sometimes waiting seconds or minutes for something to happen is already a long time. It makes us realize that Instant gratification has become an expectation; the norm rather than the exception. 

It is fair to say that one of the main reasons why we can have this instant gratification faster than ever before, is mainly due to the rise of mobile phone availability around the world. At the moment, there are currently 6.4 billion smartphone users worldwide and considering the global population is at 7.9 billion, that means a smartphone penetration rate of over 80%! For users, the mobile phone is a tool that gives us wings (literally if you’re booking a flight), and for companies across every single industry it’s seen as the perfect way to quickly engage with their clients and future customers. 

Now it’s crucial to stop and think about the following question - if we are leaning towards an Instant Future, what can companies do to provide a better experience to their customers that want everything pretty much as soon as possible, with the lowest effort made?

### Quick Apps

This is why Quick Apps exist. They don’t require installation, are very easy to find, and don’t take much mobile storage, ensuring a great experience for users, particularly the ones that want everything “yesterday”.

Why? Because with the distance of a single click, users can do the action that they are looking for, without needing to log into the app store, search for a specific app, install it, and then delete it once they’re done using it (which around 50% of the people do, according to a survey made by Medium).

On top of the above, Quick Apps are also integrable with mobile devices’ main capabilities such as global search results, push notifications, web browser searches, marketplaces, amongst other things. These integrations provide users with an even more native experience, without having them stop what they are currently doing. 

### Games for everyone, everywhere

Although it’s still early days, Quick Apps have already proved to be successful in several industries such as gaming, hospitality and travel. 
Let’s look at the gaming industry for instance. Most of the time, when someone wants to play a game on their mobile phone, they need to go to an app store, search for a game, download it, and then play. This is if they want to play on their own, because if one or more players want to play together, this process would therefore need to be repeated by each one of them.

On top of all of this, once we open an app store, the majority of the games that are shown first are those owned by a select number of publishers that have more money to invest in marketing, and so ensure visibility predominance over the rest. Meanwhile, the other publishers have to accept being on the lower end of the spectrum.

With Quick Apps, this paradigm changes completely. Several games can be integrated straight away into a mobile device, and its users will have plenty of options without having to go through the app store process. The same game could also be found in different platforms such as Facebook, Samsung, or Google Play, so users are free to play the games wherever they are, whenever they want, with whomever they feel like. Quick apps are all about giving the users the power to quickly do what they want, instead of having a few companies controlling what they see. 

### The instant wave

The most beautiful part of Quick Apps is that this is still the very beginning of it all. Any company embracing such an initiative now, is also embracing how users will interact with apps in the near and, most likely, long future. The ones that don’t, will end up missing the wave and will have to adapt to what will be seen as the standard. 

**And you, are _you_ getting ready for the Instant Future?**

---

SOURCES:
- https://medium.com/@thinkmighty/3-reasons-people-delete-your-app-d2b84a1518b5
- https://www.oberlo.com/blog/mobile-usage-statistics
- https://www.adjust.com/glossary/instant-apps/
- https://positivepsychology.com/instant-gratification/
- https://grottonetwork.com/navigate-life/health-and-wellness/instant-gratification-causes-impatience/
- https://elitecontentmarketer.com/screen-time-statistics/#:~:text=A%20study%20of%2011k%20RescueTime,minutes%20on%20their%20mobile%20devices.

---

Feel free to sign-up to the OW2 Quick App Initiative **[mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)** today.
