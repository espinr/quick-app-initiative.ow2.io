---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: false
title: Speedy Services, Less Code
subtitle: From Native to Lighter Apps 
date: 2021-09-30
tags: []
author: Martin Alvarez
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
comments: true
draft: true
---

Quick apps are the perfect complement in the new era due to their __easy discoverability__, __direct access__, __immediate response__, and growing capabilities that are on par with traditional native apps.

This transition from traditional to lighter apps is likely to attract both users and developers. Users only expect smooth and appealing products/services rather than understanding how the underlying technology works. Developers can create quick apps quickly thanks to its easy-to-use framework.

<!--more-->

## A new paradigm of lighter and quicker apps

* **Low-cost development**: 20% less code than Android apps;
* **Zero-installation**: one click to use it, less storage than Android apps;
* **Native experience**: efficient native rendering;
* **Cross-platform**: multiple devices and platforms;
* **Easy to find and promote**: endless options for discoverability and promotion;
* **High retention rate**: wide range of marketing mechanisms;
* **New opportunities**: 1.2b devices running Quick Apps.

## Resilience and Agility

As competition within the app industry intensifies, developers and publishers must be __competitive__. __Resilience__ and timely adaptation to societal changes and disruptions is essential to success in this situation.

Furthermore, businesses and developers must look for agility to fulfill the rapidly-changing users’ demands. As a result, developers will need the support of toolkits and frameworks that are easy to maintain and help them build products swiftly. These tools will have to involve less coding, less complexity in the framework, reusable components, pluggable third-party services.

## Frictionless Apps

Mobile users only interact with a significantly small number of apps installed on their devices regularly. Also, the app discovery through traditional marketplaces causes friction that causes reluctance on users. Quick Apps are the perfect alternative in the new era due to their easy discoverability, instant access, and growing capabilities with traditional native apps.

## Quick Response 

A quick app must be designed to perform one task –this specialization allows developers to maximize the user experience and guarantee quality and efficiency. The new generation of network technology would ensure the provision of the service with the lowest latency while preserving the quality, privacy, and security of the service.

## Better Tools 

Additionally, Quick Apps are based on frameworks and standards – such as HTML-like components, CSS, and JavaScript – that are popular with the developer communities, encouraging a smoother adoption of the more efficient Quick Apps.



**Read more** about quick apps on the [White Paper](/page/developers/docs/#quick-app-white-paper) and [join the Quick App Initiative](https://www.ow2.org/view/QuickApp/Participants_Form) today.
