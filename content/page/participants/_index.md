---
title: Participants
subtitle: Organizations participating in the QAI
date: 2021-12-01
section: about
comments: false
breadcrumbs: true
---

QAI was launched during in June 2021 by the following organisations:

{{< gallery caption-effect="fade" >}}
  {{< figure src="/img/participant-logos/alliancetech.png" link="./alliancetech" caption="Alliance Tech" alt="Alliance Tech" >}}
  {{< figure src="/img/participant-logos/ctic.png" link="./ctic" caption="CTIC" alt="CTIC" >}}
  {{< figure src="/img/participant-logos/famobi.jpg" link="./famobi" caption="Famobi" alt="Famobi" >}}
  {{< figure src="/img/participant-logos/frvr.png" link="./frvr" caption="FRVR" alt="FRVR" >}}
  {{< figure src="/img/participant-logos/huawei.jpg" link="./huawei" caption="Huawei" alt="Huawei" >}}
  {{< figure src="/img/participant-logos/santillana.png" link="./santillana" caption="Santillana" alt="Santillana" >}}     
{{< /gallery >}}

Recently, the following organizations also joined the QAI:

{{< gallery caption-effect="fade" >}}
  {{< figure src="/img/participant-logos/olisto.png" link="https://olisto.com" target="_blank" caption="Olisto" alt="Olisto" >}}
  {{< figure src="/img/participant-logos/startinblox.png" link="https://startinblox.com/en/" target="_blank" caption="Startin'Blox" alt="Startin'Blox" >}}
{{< /gallery >}}

Also you can check the [current participants](https://www.ow2.org/view/QuickApp/Participants) of the initiative, in the OW2 website.  

## Get involved

The QAI is open to everyone:

* Any organization or individual regardless of geographic location;
* Operating system and device vendors that implement Quick App engines, marketplaces, and other supporting tools for Quick Apps;
* Content and service providers interested in end-user interactions using Quick Apps;
* Marketing experts interested in the promotion of Quick Apps as a paradigm;
* Developers, including professionals, hobbyists, and students, interested in Web and native app technologies;
* Public institutions, including municipalities, with specific needs such as accessible services for citizens and visitors;
* Research centers and academic institutions interested in innovation through agile technologies;
* Innovative entrepreneurs and SMEs.
* ...

So, [get involved now](/page/about/#see-you-soon)!