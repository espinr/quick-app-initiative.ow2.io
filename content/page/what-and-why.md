---
title: What and why is QAI?
subtitle: About the Quick App Initiative
section: About
date: 2021-11-25
comments: false
draft: true
---

The **OW2 Quick App Initiative** explores technologies, use cases and business dynamics centered on *quick apps*, a new paradigm of light, no-install, hybrid applications for smart devices. The initiative was launched by **Huawei Europe** in collaboration with 5 European organisations; **Alliance Tech** (France), **CTIC** (Spain), **Famobi** (Germany), **FRVR** (Denmark & Portugal) and **Santillana (Spain)**. More recently, it has also been joined by **e Foundation** (France) and **Olisto** (Netherlands). The initiative is founded on the ideals of *open source* (transparency, collaboration, sharing, open to all ...), as enshrined within its [charter](https://quick-app-initiative.ow2.io/docs/charter.pdf), and as overseen by [OW2](https://www.ow2.org/view/Main/), one of Europe's preeminent open source organisations.

## Quick apps implement W3C MiniApp standards

The quick app platform is a *concrete implementation* of [standards](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp) being drafted by the [W3C MiniApp Working Group](https://www.w3.org/2021/miniapps/). This Working Group brings together leading technology companies such as Alibaba, Baidu, ByteDance, Google, Huawei, Intel, Microsoft, Rakuten, and Xiaomi.

MiniApp technology is based on a front-end Web stack (CSS, JavaScript, Web APIs ...) that leverages an MVVM design pattern architecture to provide advanced **access to device features**, and **powerful native rendering**.

Quick apps are a way to develop, package and distribute MiniApp compliant applications across platforms, facilitating the process of development through advanced UI components and predefined native APIs (e.g., push notifications, network monitoring, Bluetooth, camera ...). These enable developers to design and build usable, reliable and efficient applications *rapidly*.

- Compared to a native Android app, and looking at equivalent functions, quick apps require 20% less lines of code --> easier for the developer, lighter on download infrastructures, less impact on device storage.
- The process of updating and maintaining quick apps is more straightforward than for native apps --> developers can update their quick apps and deliver new versions to end-users in a transparent way.
- Quick apps support multi-channel distribution using deep links, marketplaces, web-links, and specific device assistants to **maximize discoverability** --> app providers can perform innovative marketing activities to promote their services and products.

## Scratching an itch

- Companies and public services increasingly need an app presence to reach their audiences, yet writing, publishing and updating apps is for many an impractical proposition. A lack of app presence robs such organisations of the rich engagement and business opportunities that apps provide.
- Publishing apps as an *output* of school courses ("hey kids, this term we're going to make an app for your phone"), or maker communities and home hobbyists, or in support of local community events, sporting meets, or even research experiments is simply not realistic. 
- App marketplaces are filled by millions of new apps each year, making it paradoxically harder for users to find apps, and frustratingly harder for businesses to get their apps seen;
- App marketplaces act as gatekeepers that undemocratically decide censorship rules, skim usage taxes whilst stifling *open* market cost dynamics, and absolutely control in-store publication and promotion opportunities.
- Sustainability goals beseech better resource efficiency, yet we download apps with abandon, consuming network bandwidth and bloating our devices;
- Study after study highlight the growing problem of  *user app fatigue*, especially for  _ad hoc_  needs.

In short, it's difficult to imagine how the *current* app dynamic can efficiently, sustainably and inclusively scale with the growing needs of smart homes, smart cities, connected transport, telehealth, and the societal shift towards online products and services.

This is where MiniApps, and in this case *quick apps*, "scratch the itch". No installation perceived by the user, liberty of distribution channels, easy to code, reduced impact on device storage and network usage, minimal access friction from a user perspective, huge discoverability and engagement opportunities from a business perspective. Perfect for ad hoc needs, in-the-moment experiences, and devices of all types.

## Where's the catch?

Mini apps and quick apps as concepts and technology platforms are widely used by consumers and developers in Asia. In China, 12 leading phone vendors support quick apps *out-of-the-box*, including Huawei, Lenovo, OnePlus, Oppo, Vivo, Xiaomi, and ZTE. 

> 1.2 billion devices support quick apps, but the vast majority of these are in China and Asia.

Despite this massive popularity and the driving needs highlight above, outside of Asia, the concept is largely unknown. Indeed, in Europe, the quick app ecosystem is still very much "greenfield". Developers, by and large, do not know the technology, businesses and consumers are unaware of the concept and its opportunities, and unlike in Asia, only Huawei devices come out-of-the-box with the necessary quick app framework integrated with the OS.

## The OW2 Quick App Initiative

The initiative aims to encourage, support and foster a vibrant and diverse quick app ecosystem in Europe and beyond that delivers on the promise of quick apps. It has 4 ambitions:
1. Raise quick app awareness within developer, business and consumer communities.
2. Build and share a commons of quick app knowledge and experience.
3. Build and share quick app tooling.
4. Support W3C MiniApp Working Group activities.

The initiative will bring together a multidisciplinary group of experts from different organizations and different countries. It will cover a wide range of industries and topics, foster innovation and entrepreneurship, advocate core values such as sustainability, resilience, user privacy and ethical use of the technology.

-	**An open community**: Any organization (public, private, academia, research ...), or individual, may participate in the activities and become a participant of the initiative;
-	**Vendor-neutral**: The initiative is focused on raising quick app concept awareness, developing tools and documentation, as well as exploring use cases from a vendor-neutral perspective;
- **Transparent and driven by group’s consensus**: Resolutions of the initiative and its activities are based on community consensus under the principles of openness and transparency;
- **Topic-oriented work**: Participants may propose specific *Task Forces* to better enable focused collaboration around a specific need or objective;
- **Open Source advocate**: The initiative is committed to the open source paradigm, fostering the production and release of Free Libre and Open Source code (OSI or FSF approved license), open documentation and open data (Creative Commons).

## How to participate

**Task Forces**
- Initiative participants have so far created Tasks Forces on Gaming, Sustainability and Education. Please feel welcome to join these, or even propose new ones.
- The Sustainability Task Force has proposed a study to examine quick apps within the apparel and footwear industries (fashion supply chain). **Could you help on this?**

**Use cases**
- Help us deepen and explore use cases across different domains (tourism, health, education ...).
- This will be materialised within the initiative as opportunity studies, proof of concepts, pilots, hackathons, events, etc.

**Community**
- Could you help organise meet-ups as a **local ambassador?**
- Could you help animate social media?

**Technologies**
-  Do you have experience in mobile app development, web technologies (JavaScript, CSS, HTML5 ...), browser engines, webview or PWA technologies?
- Could you help deepen, expand and translate online [documentation](https://quick-app-initiative.ow2.io/developers/guide/)?
- Would you be interested in collaborating on an open source quick app engine or developer tools?

**Development of pilot apps**
- Let us know if you would like to actively help develop quick app pilots.
- We are currently looking for developers to assist with various quick app pilots under discussion for 2022.

## See you soon

Everyone is welcome to join the initiative. There are no fees and no obligations to join OW2.

To become a *participant*, register here: https://www.ow2.org/view/QuickApp/Participants_Form
To *follow* initiative news, sign up to the general mailing list: https://mail.ow2.org/wws/info/quickapp

Collaboration tools:
- GitLab space: https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative
- Rocket.Chat discussion space: https://rocketchat.ow2.org/group/QAI-Town-Square

Resource spaces:
- General information: https://quick-app-initiative.ow2.io/
- Developer documentation: https://quick-app-initiative.ow2.io/developers/guide/

Social media:
- Twitter: [@OW2QuickApps](https://twitter.com/OW2QuickApps)
- Hashtag (for Twitter and LinkedIn): #QuickAppsEU
