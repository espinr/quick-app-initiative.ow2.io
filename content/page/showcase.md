---
title: Showcase
subtitle: Quick App use cases
date: 2020-04-27
section: showcase
comments: false
---

This section contains a collection of uses cases...  

{{< gallery caption-effect="fade" >}}
  {{< figure src="/quickapp/img/showcase/001-thumb.png" link="/img/showcase/001.png" caption="Quick Games" alt="Quick Games" >}}
  {{< figure src="/quickapp/img/showcase/002-thumb.png" link="/img/showcase/002.png" caption="Darts" alt="Darts" >}}
  {{< figure src="/quickapp/img/showcase/003-thumb.png" link="/img/showcase/003.png" caption="Sushi" alt="Sushi" >}}
{{< /gallery >}}

{{< load-photoswipe >}}

This section contains a collection of uses cases...  
