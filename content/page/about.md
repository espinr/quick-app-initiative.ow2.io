---
title: About us
subtitle: About the Quick App Initiative
section: About
date: 2021-11-29
comments: false
---

The **[OW2 Quick App Initiative](https://www.ow2.org/view/QuickApp/)** explores technologies, use cases and business dynamics centered on *quick apps*, a new paradigm of light, no-install, hybrid applications for smart devices. The initiative was launched by **[Huawei Europe](https://huawei.eu/)** in collaboration with 5 European organisations; **[Alliance Tech](https://alliance-tech.org/)** (France), **[CTIC](https://www.fundacionctic.org/)** (Spain), **[Famobi](https://famobi.com/)** (Germany), **[FRVR](https://frvr.com/)** (Denmark & Portugal) and **[Santillana](https://santillana.com/en/)** (Spain). More recently, it has been joined by **[e Foundation](https://e.foundation/)** (France) and **[Olisto](https://brands.olisto.com/)** (Netherlands).

The initiative is a respectful, consensus driven community, founded on the ideals of *open source* (transparency, collaboration, sharing, open to all). It operates in accordance with a community agreed [charter](https://quick-app-initiative.ow2.io/docs/charter.pdf), and is overseen by [OW2](https://www.ow2.org/view/Main/), one of Europe's preeminent open source organisations.

In August 2021, the initiative proudly gained [peer recognition](https://systematic-paris-region.org/le-hub-open-source-apporte-son-soutien-au-projet-de-lassociation-europeenne-ow2-sur-les-quick-apps/) from [Systematic Open Source Hub](https://systematic-paris-region.org/hubs-stakes/open-source-hub/?lang=en); a deep-tech innovation pole for the Paris region.

- **Learn** about quick apps in the [White Paper](https://quick-app-initiative.ow2.io/page/whitepaper)
- **Subscribe** to the [mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)
- **Get stuck in** with the [start guide](https://quick-app-initiative.ow2.io/developers/guide/getting-started.html)
- **[Ask the team a question about QAI](mailto:quickapp-team@ow2.org?subject=I%20have%20a%20question%20about%20QAI)**

## Quick apps implement W3C MiniApp standards

The quick app platform is a *concrete implementation* of [standards](https://www.w3.org/TR/mini-app-white-paper/#what-is-miniapp) being drafted by leading technology companies within the [W3C MiniApp Working Group](https://www.w3.org/2021/miniapps/). MiniApp technology is based on a **front-end Web stack** (CSS, JavaScript, Web APIs ...) that leverages **MVVM design patterns** to provide **access to device features** and **powerful native rendering**.

> Quick apps are a way to develop, package and distribute MiniApp compliant applications across platforms

Quick apps facilitate the process of development through advanced UI components and predefined native APIs (e.g., push notifications, network monitoring, Bluetooth, camera ...). These enable developers to design and build usable, reliable and efficient applications *rapidly*.

- Compared to a native Android app, and looking at equivalent functions, quick apps require 20% less lines of code --> **easier for the developer**, lighter on download infrastructures, less impact on device storage.
- The process of updating and maintaining quick apps is more straightforward than for native apps --> developers can **easily update** their quick apps and efficiently deliver new versions to end-users.
- Quick apps support multi-channel distribution using deep links, marketplaces, web-links, and specific device assistants --> app providers can **maximize app discoverability** and perform innovative marketing activities to promote their services and products.

## Scratching an itch

- Companies and public services increasingly need an app presence to reach their audiences, yet writing, publishing and updating apps is for many an impractical proposition. A lack of app presence robs such organisations of the rich engagement and business opportunities that apps provide.
- Publishing apps as an *output* of school courses ("hey kids, this term we're going to make an app for your phone"), or maker communities and home hobbyists, or in support of local community events, sporting meets, or even research experiments is simply not realistic. 
- App marketplaces are filled by millions of new apps each year, making it paradoxically harder for users to find apps, and frustratingly harder for businesses to get their apps seen;
- App marketplaces act as gatekeepers that undemocratically decide censorship rules, skim usage taxes whilst stifling *open* market cost dynamics, and absolutely control in-store publication and promotion opportunities.
- Sustainability goals beseech better resource efficiency, yet we download apps with abandon, consuming network bandwidth and bloating our devices;
- Study after study highlight the growing problem of  *user app fatigue*, especially for  _ad hoc_  needs.

In short, it's difficult to imagine how the *current* app dynamic can efficiently, sustainably and inclusively scale with the growing needs of smart homes, smart cities, connected transport, telehealth, and the societal shift towards online products and services.

This is where MiniApps, and in this case, **quick apps "scratch the itch"**. No installation perceived by the user, liberty of distribution channels, easy to code, reduced impact on device storage and network usage, minimal access friction from a user perspective, huge discoverability and engagement opportunities from a business perspective. Perfect for ad hoc needs, in-the-moment experiences, and devices of all types.

## Where's the catch?

Mini apps and quick apps as concepts and technology platforms are widely used by consumers and developers in Asia. In China, 12 leading phone vendors support quick apps *out-of-the-box*, including Huawei, Lenovo, OnePlus, Oppo, Vivo, Xiaomi, and ZTE. 

> 1.2 billion devices support quick apps, but the vast majority of these are in China and Asia.

Despite this massive popularity and the driving needs highlighted above, outside of Asia the concept is largely unknown. Indeed, in Europe the quick app ecosystem is still very much "greenfield". Developers, by and large, do not know the technology, businesses and consumers are unaware of the concept and its opportunities, and unlike in Asia, only Huawei devices come out-of-the-box with the necessary quick app framework integrated with the OS.

## The OW2 Quick App Initiative

The initiative aims to encourage, support and foster a _vibrant and diverse_ quick app ecosystem in Europe (and beyond) to deliver on the promise of quick apps.<BR>It has 4 ambitions:
1. Raise quick app awareness within developer, business and consumer communities;
2. Build and share a commons of quick app knowledge and experience;
3. Build and share quick app tooling (for example, tools, code templates, plugins for IDEs, test-beds, etc.);
4. Support W3C MiniApp Working Group activities.

The initiative brings together a **multidisciplinary group of people** from **different organizations and countries**. It covers a wide range of industries and topics, fosters innovation and entrepreneurship, advocates for core values such as sustainability, resilience, user privacy and ethical use of the technology.

-	**An open community**: Any organization (public, private, academia, research ...), or individual, may participate in the activities and become a participant of the initiative;
-	**Vendor-neutral**: The initiative is focused on raising quick app concept awareness, developing tools and documentation, as well as exploring use cases from a vendor-neutral perspective;
- **Transparent and driven by group’s consensus**: Resolutions of the initiative and its activities are based on community consensus under the principles of openness and transparency;
- **Topic-oriented work**: Participants may propose specific *Task Forces* to better enable focused collaboration around a specific need or objective;
- **Open Source advocate**: The initiative is committed to the open source paradigm, fostering the production and release of Free Libre and Open Source code (OSI or FSF approved license), open documentation and open data (Creative Commons).

## How to participate

**Task Forces**
- Initiative participants have so far created Tasks Forces on _Gaming_, _Sustainability_ and _Education_. You are welcome to join these, or even propose new ones.
  - The Sustainability Task Force has proposed a [study](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/blob/master/task-forces/TF%20Sustainability/QAI_Task_Force__Sustainable_Fashion_Industry_Model_.pdf) to examine quick apps within the apparel and footwear industries (fashion supply chain). **Could you help on this?**

**Use cases**
- Help us deepen and explore use cases across different domains (tourism, health, education, sport, commerce, ...).
- Use case exploration is materialised within the initiative as opportunity studies, proof of concepts, pilots, hackathons, events, etc.

**Community**
- Help us to organise meet-ups as a **local ambassador**
- Help us social media.

**Technologies**
- Do you have experience in mobile app development, web technologies (JavaScript, CSS, HTML5 ...), browser engines, webview or PWA technologies?
- Could you help deepen, expand and translate online [developer documentation](https://quick-app-initiative.ow2.io/developers/guide/)?
- Would you be interested in collaborating on an open source quick app engine, or developer tools?

**Development of pilot apps**
- Let us know if you would like to actively help develop quick app pilots.
- We always welcome developers to assist with the various quick app pilots under discussion.

## See you soon!

Everyone is welcome to join the initiative. There are **no fees** and **no obligations to join OW2** (although they always appreciate having new members).

- To **join**, register here: https://www.ow2.org/view/QuickApp/Participants_Form
- To **follow**, sign up to the mailing list: https://mail.ow2.org/wws/info/quickapp
- To **ask the team a question**, email us here: [quickapp-team@ow2.org](mailto:quickapp-team@ow2.org?subject=I%20have%20a%20question%20about%20QAI)

Collaboration tools:
- GitLab space: https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative
- Rocket.Chat discussion space: https://rocketchat.ow2.org/group/QAI-Town-Square

Resource spaces:
- General information: https://quick-app-initiative.ow2.io/
- Developer documentation: https://quick-app-initiative.ow2.io/developers/guide/

Social media:
- Twitter: [@OW2QuickApps](https://twitter.com/OW2QuickApps)
- Hashtag (for Twitter and LinkedIn): #QuickAppsEU


<!--
The **Quick App Initiative** (abbreviated simply to "_QAI_") is a neutral place to foster awareness, develop documentation and use cases, collaborate on tooling and coordinate grassroots activities in **support of a Quick App implementation** of the [W3C MiniApp standard](https://www.w3.org/2021/miniapps/). It enables the **active collaboration** of different stakeholders, from device vendors to app/content providers, within a neutral, Open Source based structure.

- **Learn** about Quick Apps in the [White Paper](https://quick-app-initiative.ow2.io/page/whitepaper).
- **Subscribe** to the [mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info).
- **Access** our [code repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative).
- **Join** the initiative through the [registration form](https://www.ow2.org/view/QuickApp/Participants_Form).
- **Read** the foundations of the _QAI_ in the [charter](/docs/charter.pdf).

## Some context

In January 2021, the World Wide Web Consortium ([W3C](https://www.w3.org)), supported by the leading Web technology companies, launched the [MiniApp Working Group](https://www.w3.org/2021/miniapps/). This group aims to define standards for a new paradigm of light hybrid applications called *MiniApps*. The MiniApp technology is based on front-end Web technologies like **XML**, **CSS**, **JavaScript**, and **Web APIs**, yet offers advanced access to device features and supports native rendering. Importantly, *MiniApps* are designed to run on a runtime engine close to the device OS.

The **Quick App Initiative**, [hosted by OW2](https://www.ow2.org/view/QuickApp/), promotes a **concrete implementation** of the abstract [W3C MiniApp standard](https://www.w3.org/2021/miniapps/), allowing light applications in native environments for smart devices.

## Our objectives

The initiative explores vertical applications where Quick Apps have the potential to be part of a solution, as well as transversal activities that further core technologies across verticals, including (as appropriate) **usability**, **accessibility**, **sustainability**, **privacy**, **security**, and **ethical standards**.

The initiative has four key aims:
1. **Raise awareness** and understanding about Quick Apps in business and developer communities.
2. Develop a commons of **technical knowledge**, guidelines and best practices.
3. Feed into the emerging **[W3C MiniApp standard](https://www.w3.org/2021/miniapps/)** (and act as a dissemination channel).
4. **Develop Open Source code** about Quick Apps. For example, tools, code templates, plugins for IDEs, test-beds, Etc.

## A multi-party initiative

The Quick App initiative has been launched by the following organisations:
* CTIC
* European Alliance for Technology Development (Alliance Tech)
* Famobi
* FRVR
* Huawei Europe **(Initiative chair)**
* RSI Foundation
* Santillana

The initiative is **transparent and meritocratic** and governed by a [charter](/docs/charter.pdf) that has been drafted and peer-reviewed by the launch participants above and the [OW2 Board](https://www.ow2.org/view/About/OW2_Governance#HBoardMembers28Year202029). 

In brief, this initiative is based on the following foundations:
- **Respectful, open community**. Any organization or individual from any country, race, or religion is welcome to participate in the activities and become a participant of the initiative;
- **Not pay to play**. Interested parties do not need to join OW2 (although highly encouraged), nor does the initiative require a “membership” fee.
- **Multi-stakeholder community**. Public and private organizations, academia, and individuals are warmly encouraged to enrich the community ecosystem;
- **Vendor-neutral**. The initiative is focused on developing tools, documentation, training, use cases, code examples, and building awareness of Quick Apps as a technology platform from a vendor-neutral perspective;
- **Transparent and driven by consensus**. Group resolutions and their activities are based on collective understanding and agreements under openness and transparency principles.


## Projects and deliverables

The initiative fosters the authoring of **documents** and the development of dedicated **tools** and runtime **software engines**. 

The documents and the Open Source projects developed within the initiative will be hosted in the [initiative's repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative) within the OW2 code base.

Visit the developer sections to see all the [open source projects](/page/developers/code) and [documents](/page/developers/docs) already created.  


## Events

Launched at [OW2con'21](https://www.ow2con.org/view/2021/) in June 2021, the Quick App Initiative promotes its activities at various events as it makes progress.

See the list of [past and upcoming events](/events/) related to the Quick App Initiative.  

## You are welcome to participate

The Quick App Initiative is open to:
* Any organization or individual regardless of geographic location;
* Operating system and device vendors that implement Quick App engines, marketplaces, and other supporting tools for Quick Apps;
* Content and service providers interested in end-user interactions using Quick Apps;
* Marketing experts interested in the promotion of Quick Apps as a paradigm;
* Developers, including professionals, hobbyists, and students, interested in Web and native app technologies;
* Public institutions, including municipalities, with specific needs such as accessible services for citizens and visitors;
* Research centers and academic institutions interested in innovation through agile technologies;
* Innovative entrepreneurs and SMEs.
* ...

All the participants in the Quick App Initiative agree to be bound by [the charter](/docs/charter.pdf) and are required to follow the OW2 governing laws and policies, not least the [OW2 Code of Conduct](https://www.ow2.org/view/Membership_Joining/Code_of_Conduct).


## Join


**[Join us](https://www.ow2.org/view/QuickApp/Participants_Form)** today and stay in touch with us and get more information about Quick Apps and the Quick App Initiative:

- **Subscribe** to the [mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info).
- **Learn** more [about the Quick App Initiative](https://quick-app-initiative.ow2.io/page/about/).
- **Access** our [code repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative).
- **Check the initiative's foundations** in the [charter](/docs/charter.pdf)

-->
