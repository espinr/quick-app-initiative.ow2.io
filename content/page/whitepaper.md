---
title: Quick App 
subtitle: White Paper
date: 2021-06-15
section: whitepaper
comments: false
---

- Issued: June 2021 (Quick App Initiative)
- Also in [PDF version](/docs/Quick_App_White_Paper.pdf "Quick App White Paper in PDF format") (1.6 MB).
- Authors: ALVAREZ-ESPINAR M., BISHOP K., PATERSON C. 
- [Acknowledgments and feedback](#acknowledgments-and-feedback)
- Document licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/ "Attribution 4.0 International") License

---

# Contents

- [Executive summary](#executive-summary)    
- [Societal resilience and speedy services](#societal-resilience-and-speedy-services)
  - [Technology-driven societal resilience](#technology-driven-societal-resilience)
  - [New technologies towards digital sovereignty](#new-technologies-towards-digital-sovereignty)
  - [Users want experiences, not exhausting apps nor websites](#users-want-experiences-not-exhausting-apps-nor-websites)
  - [What is a Quick App?](#what-is-a-quick-app)
  - [High-level features](#high-level-features)
  - [Product's whole lifecycle support](#products-whole-lifecycle-support)
  - [High performance, easy coding, advanced services](#high-performance-easy-coding-advanced-services)
  - [Types of Quick Apps](#types-of-quick-apps)
  - [Business opportunities](#business-opportunities)
  - [Suitable use cases and scenarios](#suitable-use-cases-and-scenarios)
- [How to make Quick App a global success](#how-to-make-quick-app-a-global-success)
  - [OW2 Quick App Initiative](#ow2-quick-app-initiative)
  - [Open and international standards](#open-and-international-standards)
  - [European digital strategy alignment](#european-digital-strategy-alignment)
- [Case studies](#case-studies)
  - [Case study: Games](#case-study-games)
  - [Case study: Accommodation](#case-study-accommodation)

---

# Executive Summary

Our society is experiencing a remarkable period of digitalization that is transforming business and enhancing lives in a constant movement of exploration, re-invention, evolution, and adaptation; to changes in customer habits, to new trends and market needs, to emerging new social, medical and technical advances, to ecological challenges, and even to unexpected events. Such changes bring with them new ways of doing things, new ways of looking at things, new opportunities for advancement, and new challenges for regulation. In the face of these multiple drivers of change, the need for **agile development frameworks** that enable a new breed of mobile-first, low friction applications is greater than ever. Quick App is a powerful example of just such a technology. Quick App empowers businesses of all sizes, public bodies, and even individuals to build, deploy, and update rich applications with minimum effort.

Quick App is an implementation of the emerging [W3C MiniApps](https://www.w3.org/2021/miniapps/) standard that provides **a framework for mobile application development based on**  **widely known front-end web technologies**. It enables developers to create "light" applications more easily with significant access to a host devices' native resources and services. Imagine, for example, marketers and publishers using a Quick App platform to deliver products and services with an accelerated time-to-market process and tools for product lifecycle management, including promotion, user acquisition, monetization, and user retention.

As lighter, low friction versions of larger native apps, Quick Apps are an excellent channel to attract and then transition potential customers from ad hoc interactions to deeper ongoing relations. They are ideal for **infrequent services that cover less complex tasks**. They also make great entry points to premium products. The end-user has a **fast time-to-value**, **near-instant gratification experience**, and due to the lack of app acquisition and launch friction, the user experience is also more natural and more immersive.

Quick App is currently supported by over 1.2 billion devices worldwide. It aims to become a ubiquitous universal platform thanks to **open standards**, an international multi-party [W3C Working Group](https://www.w3.org/2021/miniapps/), and community spread fostered through the vendor-neutral [OW2 Quick App Initiative](https://quick-app-initiative.ow2.io/); a European based forum where industry, academia, and individuals can discuss real-world challenges, collaborate on Quick App enablers and encourage worldwide Quick App awareness and best practices.

> Quick Apps is a technology that allows developers to create targeted mobile applications quickly while addressing user experience, privacy, and digital sovereignty through open standards, multiplatform support, and vendor neutrality.

# Societal resilience and speedy services

## Technology-driven societal resilience

Our society is evolving quickly. Information and Communications Technologies (ICT) are transforming our communities, changing business models, and making industries more efficient whilst also connecting organizations and people for a more empowered daily life. Recent trends include a strong acceleration of service digitization, increased adoption of software applications for personal interactions, the growth of online commerce, more working from home, an increased need to staying informed, and the growth of fragmented leisure time. There has been a marked change in people's mindset about using online platforms and services, creating a global dependence on the Internet, smart devices, and apps.

The capacity and **resilience to adapt** to new challenges and requirements have become the "new normal" in society. People of all ages and conditions are being compelled into using social networks, online payments, and contactless interfaces, like scanning QR-codes to read a restaurant menu. This shift has boosted the development of new apps on our devices to support transactional services like payment of street parking, purchasing public transport tickets, or ordering takeaway food. These services are contextualized within a short time and oriented to achieving a specific task.

![Phone scanning a QR code](/img/whitepaper/fig1.png)
_Figure 1. Order and pay by phone_

So as our lives inexorably shift online, we are increasingly confronted with a paradoxical situation: companies and public services need an app presence to gain audience and satisfy "customers"; the addition of millions of apps to marketplaces each year is leading to market saturation and filled devices; fatigued consumers never seem to have the right app, at the right time, in the right place.

Driven by increasing demands for more apps, better apps, contextualized apps, **agile development frameworks**, like the low-code paradigm, are gaining traction in all sectors. Quick App is a light app platform with a similar approach that facilitates developers to build reusable functional components for concrete tasks in specific fields, reducing effort and time.

## New technologies towards digital sovereignty

Developers love transparency and flexibility in product distribution. The freedom to choose the right marketplace and delivery platform for their target audience are essential for achieving digital sovereignty.

Recent regulatory movements worldwide are trending towards enforcing fairer trading practices in app marketplaces and stores. Developers may soon be given more **flexibility and choice** in app distribution, which will allow more innovative business models. These new scenarios aim to require transparent platforms, enabling **fair competition between SMEs and large corporations**. Within the near term, the successful coexistence of multiple app marketplaces, and other application distribution methods may well become commonplace.

In this thriving digital market, developers need technologies backed by open standards that lower technical and strategical barriers so that they can focus on the creative process and explore innovative business models with the least effort.

Furthermore, motivated by growing privacy concerns and societal requirements to protect users, the online advertising industry is evolving quickly. Thus, marketers and advertisers are looking for new and innovative mechanisms to provide free (ad-paid) online content and services on top of advertising-driven models while protecting user's privacy.

> Quick Apps is a competitive technology that addresses **user's privacy** and **digital sovereignty**  through **open standards**, **multiplatform support**, and **vendor neutrality**.

## Users want experiences, not exhausting apps nor websites

With devices overwhelmed by heavy and complex applications, users increasingly only use a small set of their installed apps. The exponentially growing number of apps in the various app marketplaces and the tedious installation and registration process are making consumers reluctant to install new applications and try new services. The upshot is that user fatigue with traditional apps, the need for quicker interactions in unplanned situations, and the expanding variety of digital devices that people use (wearables, car head units, home appliances,…) are stimulating a sea-change in how companies deliver experiences to smart devices.

**Users want to enjoy speedy, sound, and convenient experiences**, not waste their time searching across millions of websites or within huge app marketplaces and stores. But even when they've found a potentially exciting app, they're then frustrated by download and install time and device resource usage. And all that to try a new application that is likely to be deleted after the first use. In today's world of high choice, high speed, high churn, microtransaction, on-the-go hustle and bustle, businesses need to offer rich, value-creating experiences that answer users' needs instantly, not in five minutes. These end-to-end low friction experiences simply cannot be delivered by the traditional search-download-use app paradigm.

App creation, discovery, and installing issues lead many organizations to position their websites as their primary sales funnel. This approach has merit but is not the best route to a great user experience due to the technical constraints of web browsers, as software applications that cannot fully exploit the advanced features of devices.

This situation is motivating publishers and marketers to look for innovative ways to acquire users and enlarge their market through direct interaction with their application services that make the most of device capabilities users happen to have.

## What is a Quick App?

> Quick App is a new concept of **light native-like applications**, or mini-apps, that **do not require installation** and **run directly on the operating system**, offering high performance and a rich user experience.

Quick App is a vendor-neutral framework for mobile application development based on the emerging [W3C MiniApps standards](https://w3c.github.io/miniapp/white-paper/). From a developer perspective, it relies on the widely understood **front-end web technology stack**, including an **HTML** -like markup language, **JavaScript**, and **CSS** stylesheets. Quick App enables developers to quickly and efficiently create light applications with significant access to a device's native resources and services (unlike browser-based frameworks).

It is estimated that Quick Apps generally require 20% less code when compared to functionally equivalent native Android apps. And the close integration of the framework runtime engine with the device enables applications to use services at the operating system level, achieving high performance while consuming fewer resources. That is to say, quicker for developers to write, quicker for users to download, quicker and easier for devices to run.

Quick Apps can exploit many of the amazing capabilities of smart devices to create great user experiences. Users can discover and access Quick Apps through different means, including the device's global search results, push notifications, web browser searches, marketplaces, physical QR-codes, file systems, in-app advertisements, and URLs in general.

With billions of devices worldwide, multiple device manufacturers today (including Huawei, OPPO, OnePlus, and Xiaomi) integrate the Quick App framework into their mobile operating systems, enabling access for Quick App developers and publishers to multiple markets. And, given that Quick App developers write software code to a common Application Programming Interface (API) set, this enables a **"write-once, run anywhere"** ecosystem dynamic. This is a powerful incentive that is a win-win for developers, businesses, and users.

> Quick App is a new concept of light native-like applications that do not require installation and run directly on the operating system, offering high performance and a rich user experience.

## High-level features

The paradigm of light apps offers several benefits both for end consumers and developers. Concretely, the Quick App platform is characterized by, but not limited to, the following features.

For **customers**:

- **Native look and feel**. Quick Apps use predefined customizable components to build user interfaces that can closely resemble elements commonly used in native applications.
- **Easy discoverability**. Quick Apps may be listed in app marketplaces, websites, banners and ads, widgets, referred by messages in social media or emails, and QR codes.
- **No installation is required**. Once users discover a Quick App, they only need to tap on the link or scan the QR code to access and run the application, saving the user time and device storage.
- **Native performance**. The Quick App framework has optimized access to the native features of the device and uses multiple computation threads to maximize efficiency during user interaction.
- **User first**. Although users do not experience an installation process, the Quick App platform guarantees a customized interaction with the application, preserving the latest status and setup of the application.

> _Quick Apps require 20% less code than native Android apps._

For **developers**:

- **Easy to code**. The Quick App platform has a declarative User Interface (UI), with predefined components (e.g., navigation bars, lists, tabs, buttons, and canvas) to design reactive screens that match the device's characteristics. Developers can port HTML5 apps to Quick Apps in less than one hour for most applications.
- **Multilingual**. Quick Apps include internationalization options to localize content for reaching global audiences.
- **Access to advanced system services**. Quick Apps make the most of the user's device through direct access to advanced native services such as push notifications, internal sensors, storage, calendar, and contacts whilst protecting the security and data privacy of the user.
- **Flexible packaging and delivery**. Quick Apps are multiplatform and may be run on different devices. Developers can distribute Quick Apps to various app marketplaces and stores, or share the app package through direct downloads from web repositories or even email.
- **Transparent updates**. Since Quick Apps are non-installable, the Quick App framework manages the whole update process of these applications. Developers can create different application versions, documenting them in the distribution directory or app marketplace. These updates are then reflected when the end-user re-interacts with the given Quick App without any intermediate steps (except when changes affect the user's privacy or the system privileges).
- **High user conversion rate.** The high discoverability and the absence of installation offer publishers greater possibilities to engage new customers using Quick Apps than with traditional apps.

## Product's whole lifecycle support

Through the Quick App platform, content and service providers can manage the whole lifecycle of a product or service, dealing with **promotion**, **user acquisition**, **monetization**, and user **retention**.

**Discoverability**

Quick Apps can be distributed through any means, including app marketplaces and stores, code repositories, and local file systems. This flexibility allows publishers and marketers to choose the most suitable channels to support promotional campaigns specifically designed for their application's target audience.

Quick Apps can be listed and promoted through any online mechanism, including links in websites, deep links from other apps, SMS/MMS texts, search engine results, social media posts, and listed in app marketplaces. Another strong point is their ability to use the device's features to facilitate findability through push notifications, widgets on the device dashboard, device global search results, digital assistants, and recently used apps.

In addition to this, publishers can use QR codes to trigger Quick Apps through offline print or video materials.


![Discovery methods in Quick Apps](/img/whitepaper/fig2.png)
_Figure 2. Discovery methods in Quick Apps_ 

**User acquisition**

The great discoverability of Quick Apps fosters user acquisition. Users only have to tap on a Quick App link or scan a QR code to interact with the application in seconds. This direct access without any tedious intermediate installation process makes Quick Apps a promising platform for attracting new users in specific contexts.

After accessing a Quick App, the system will inform users about the privacy features required to run it (if any are needed). The permission authorization dialogues are concise and straightforward, letting users understand the privacy and security implications and accept or deny them with a simple click. Other commonly used services directly integrated into the Quick Apps platform, like authentication and payment, enabling a comfortable and frictionless starting point to engage users.

> In 2020, the number of active Quick App users rose by 37% [^1]

[^1]: Source: [Quick App Alliance](https://bbs.quickapp.cn/forum.php?mod=viewthread&tid=3716)

**Sustainability**

Apart from making the most of device capabilities and using a common API set, the different vendors of Quick App supporting devices can implement third-party services to create smooth user experiences that deliver advanced vendor-specific services. These third-party services use underlying vendor-specific APIs that enable Quick Apps to use functions such as in-app purchases (iAP), advertising, or rewarded ad videos for guaranteeing the sustainability of the service or product, usually provided free of charge.

![One-click payment, in-app advertising](/img/whitepaper/fig3.png)
_Figure 3. One-click payment, in-app advertising_ 

> Advertising monetization can be grown significantly by using Quick Apps. For example, Quick Apps allowed one game to increase ad revenue by over 330% and daily traffic revenue by 128 times [^1]

**User Retention**

Aside from an easy, more natural access and launch process, the Quick App platform provides publishers with mechanisms to engage users, fostering recurring interaction with the application. These mechanisms can be either passive (e.g., creating home-screen shortcuts that store user preferences and last session data) or active (e.g., publishers can send direct push notifications with promotional messages or even publish advertising banners).

This improved user experience, also brought about by high performance and reduced resource utilization –i.e., less power, memory, and storage consumption– contributes to user engagement. In 2020, user retention of Quick Apps grew by over 70% globally, according to [Quick App Alliance's statistics](https://bbs.quickapp.cn/forum.php?mod=viewthread&amp;tid=3716).

> In 2020, user retention of Quick Apps grew by over 70% globally.

![Dialog to create a home-screen shortcut](/img/whitepaper/fig4.png)
_Figure 4. Dialog to create a home-screen shortcut_ 

![Tailored push notifications](/img/whitepaper/fig5.png)
_Figure 5. Tailored push notifications_ 


> ¨In innovation projects, the quick deployment of developments is a decisive factor and very intensive in time. Quick App helps us to overcome this limitation.¨ - Pablo Coca, Business Development &amp; Operations Director at CTIC/W3C Spain

## High performance, easy coding, advanced services

The optimized Quick App framework enables developers to create high-performance applications leveraging most capabilities of an end user's device. Its **multithread** approach, based on **two different engines**, one for visualization and rendering and another dedicated to the business logic (JavaScript engine), allows Quick Apps to deliver high-performance services that adapt to the user's demands whilst accessing operating system services.

![Quick App platform components](/img/whitepaper/fig6.png)
_Figure 6. Quick App platform components_

The Quick Apps platform is closely coupled to the **device's GPU (Graphics Processing Unit)**, allowing it to display animations and graphics efficiently. In addition, advanced machine learning algorithms in the app's code, like image processing or speech recognition, can utilize the **NPU (Neural Processing Unit)** of devices. The exploitation of these dedicated abilities powers smooth user interactions and immediate delivery of content. These aspects are critical in high-performance applications that involve video and image processing, evidencing games on Quick Apps (or Quick Games) as a perfect ally for light game developers.

> Developers use built-in UI features such as rating, user dialogues, push notifications, touch sliders, tabs, and maps.

Beyond the above-mentioned capabilities, the Quick App platform allows developers to apply **predefined UI components** to make the coding process faster and easier than native app development or web programming. These UI components extend essential HTML elements, like anchors, lists, and containers, and offer advanced features such as ratings, user dialogues (_popups_), touch sliders (_swiper_), tabs, and maps. Programmers can even create customized components for **further reusability**.

![Quick Apps UI components](/img/whitepaper/fig7.png)
_Figure 7. Quick Apps UI components_

Furthermore, the Quick App platform provides developers with **advanced APIs similar to native apps**, overcoming the limitations of similar technologies that run on hosting apps like web browsers. Developers may access and control services like screen brightness, battery status, audio volume, and network connectivity. Also, Quick Apps may run in background processes, manage on-device storage, and interact with the user via system dialogs, push notifications, and placing widgets within the device.

Quick App vendors can also extend existing APIs to implement advanced services such as analytics, speech recognition, one-click payments, advertising, and user authentication.

![Quick App platform list of services](/img/whitepaper/fig8.png)
_Figure 8. Quick App platform list of services_

## Types of Quick Apps

Predefined templates and components allow developers to build high-quality Quick Apps to cover any specific purpose and domain.

**Games** are good candidates to be implemented as Quick Apps. **HTML5** games, including those created through popular game engines like _Cocos_, _Laya_, and _Egret_, can be converted easily into Quick Games, resulting in significant performance optimization and less power consumption.

Usually, Quick Apps run as standalone services in dedicated windows (like native applications), but **Quick Apps can also be displayed as widgets**. Widgets are Quick App containers embedded in specific places like the device home-screen to deliver practical information to the user in particular contexts such as public transport alerts, car park payment reminders, and other interaction opportunities with future services.

> Developers may access and control services like screen brightness, battery status, audio volume, and network connectivity.

![Quick Games and Widget](/img/whitepaper/fig9.png)
_Figure 9. Quick Games and Widget_

## Business opportunities

The rapid digitation of companies and public bodies is causing a considerable increase in the number of applications listed in app marketplaces and stores, flooding them (and user's devices) with apps. This "application overload" is driving a growing user reluctance to install new apps.

The Quick App paradigm brings new opportunities for businesses to create better and more **innovative services** that engage users who feel fatigued with the traditional process of app discovery and installation.

With less coding effort and access to device resources, Quick Apps offers businesses a powerful **minimum time-to-value** route. This can be especially important when dealing with unexpected events, or for smaller companies with less development capacity, or for capitalizing on time-dependent opportunities.

And, since customers and users can use these new services directly, without intermediate barriers, service and content providers can leverage this dynamic to more easily engage _ **new** _ **customers** and create _ **new** _ **business opportunities**.

> These particularities make Quick Apps an ideal candidate for specific domains, including shopping, productivity, lifestyle, news, and low-graphic games.

## Suitable use cases and scenarios

Today's Quick Apps demonstrate both how powerful and flexible the Quick App platform is, but it would be amiss not to highlight how the platform is also ideal for infrequent services that cover simple on the move tasks that require a short execution/interaction time. As light versions of potentially more sophisticated native apps, Quick Apps are excellent **entry points to introduce customers to "full-fat" premium products**. The end-user has a fast time-to-value, near-instant gratification experience, and due to the lack of app acquisition and launch friction, the user experience is also more natural and immersive.

**Shopping**

Online marketing and payment ease are two critical aspects of the retail market, where advertising and marketing campaigns drive user acquisition, and the shopping cart abandonment rate is extraordinarily high. The Quick App platform offers marketers and publishers a series of **tools to engage new customers** and facilitate the checkout process. Thus, retailers can adapt the application experience to the interests of their potential customers, delivering tailored direct marketing messages through system notifications, advertisements, or physical QR-codes and letting interested users purchase in fewer trustful steps.

**Productivity**

Quick Apps can support users with infrequent **tasks in unexpected situations**. For example:

- At a physical shop, customers could scan QR-codes and virtually try on clothes or make-up through an augmented reality app;
- A simple Quick App could help hotel guests adjust their room's comfort preferences (light, temperature, music…);
- An on-site customer satisfaction survey could be made easily available after a service;

**Public services**

The deployment of core public sector services online has greatly accelerated as governments seek to maximize budget efficiency, as citizens demand better and easier access to public data and public services, and as public bodies look to communicate and **interact with citizens** remotely. Governments worldwide have implemented new applications based on Quick Apps for requesting appointments, manage utilities, pay taxes, and other advanced smart-city services.

**Local transport**

Getting information about public transport, such as the status of the service or timetables, and booking tickets on the go is another example of touchless services that can be easily implemented on Quick Apps. Local companies in Mexico, China, and India have created location-aware Quick Apps that allow customers to **book tickets** and **be informed** about alerts and disruptions of service. Customers save time and avoid queues, increasing efficiency in the process and maximizing the user experience.

**Bars and restaurants**

Like local transport, Quick Apps enable bars and restaurants to create touchless services for their customers. Clients can **check the menu**, **order**, and **pay** through their smartphone or even car head unit if they're in a drive-through. These businesses can also complement such services with targeted marketing campaigns, offering vouchers and discounts to increase customer fidelity.

![Quick Apps for public health maps and local transport](/img/whitepaper/fig10.png)
_Figure 10. Quick Apps for public health maps and local transport_

**Games**

Casual games are perfect candidates to be implemented as Quick Games: easy to discover and play, **great interaction with minimum latency**, and access to in-app purchases and advertising (including reward videos) that allow a free-to-access game experience whilst ensuring a sustainable revenue model for the developer. Quick Games offer one-click authentication and social interactions for user engagement. Quick Games could also be used as a reduced feature-set free-to-play version of a complete pay-to-play game, allowing users to try the game _instantly_ without passing through a marketplace.

![Quick Games](/img/whitepaper/fig11.png)
_Figure 11. Quick Games_

**Travel, information, entertainment, education**

As with native applications, Quick Apps can implement any specific requirement from industry or society. By providing tools for **real-time data**, **alerts**, and **direct user interaction**, the platform offers developers a wide range of possibilities to create high-quality apps that cover innovative business cases.

# How to make Quick App a global success

Openness and standards are key elements for encouraging widespread technology adoption and success based on a level playing field. Quick Apps, initially designed as an industrial "definition" by the Quick App Alliance in China, and **currently supported by over 1.2 billion devices**, is now evolving into a full international standard fostered by the [MiniApp Working Group](https://www.w3.org/2021/miniapps/) within the [World Wide Web Consortium (W3C)](https://www.w3.org/). This W3C Working Group is drafting MiniApp specification standards, of which Quick Apps are a concrete implementation. This **open standardization** process will foster new Quick App engines enabling more platforms to host Quick Apps and driving the technology towards becoming a truly universal platform. Success is only possible with the support of the developer community, and this community loves open source. The [OW2 Quick App Initiative](https://quick-app-initiative.ow2.io/), spearheaded by international companies, SMEs, and individuals, will become the cornerstone of Quick Apps outreach into Europe and beyond.

> The Quick App Initiative is the one-stop platform for Quick Apps, a forum to discuss real-world challenges while maximizing technology uptake.

## OW2 Quick App Initiative

The [OW2 Quick App Initiative](https://quick-app-initiative.ow2.io/) will be the go-to place for the growing Quick Apps community outside of China. It will be an open and meritocratic forum to discuss and collaborate on solving real-world challenges confronting the growth of the Quick App technology and platform, feed into the [W3C MiniApp](https://www.w3.org/2021/miniapps/) standard, develop Quick App best practices, knowledge, and tooling, and generally encourage this still nascent global platform ecosystem. The initiative, driven by multidisciplinary, international experts covering a wide range of industries and topics, will foster innovation and entrepreneurship and promote core values such as sustainability, resilience, user privacy, and ethical use of Quick Apps technology.

The [Quick App Initiative](https://quick-app-initiative.ow2.io/) established as a not-for-profit interest group within [OW2](https://www.ow2.org/) (a respected global open-source software organization in the heart of Europe), is based on the following foundations:

- **Respectful, open community**. Any organization or individual from any country, race, or religion is welcome to participate in the activities and become a participant of the initiative;
- **Not pay to play**. Interested parties do not need to join OW2 (although highly encouraged), nor does the initiative require a "membership" fee.
- **Multi-stakeholder community**. Public and private organizations, academia, and individuals are warmly encouraged to enrich the community ecosystem;
- **Vendor-neutral**. The initiative is focused on developing tools, documentation, training, use cases, code examples, and building awareness of Quick Apps as a technology platform from a vendor-neutral perspective;
- **Transparent and driven by consensus**. Group resolutions and their activities are based on collective understanding and agreements under openness and transparency principles.

The initiative explores vertical applications where Quick Apps may be part of a solution and transversal activities to further core technologies that apply across verticals, such as usability, accessibility, sustainability, privacy, security, and ethical standards.

Participation in the [Quick App Initiative](https://quick-app-initiative.ow2.io/) is **open to any individual or business interest**; those wishing to assist with awareness-raising or developing open-source tools and reference documents, to those interested in supporting the technology ecosystem (integrators, service providers, consultants…), or leverage the technology (app publishers, marketing companies, public bodies, tourism enterprise…). Regardless of geographic location and sector, any participant may propose and collaborate on different projects and activities within the initiative.

> "_Let's bring new standards to the market. Let's facilitate content production for quick apps." - Ilker Aydin, Famobi CEO_

## Open and international standards

### W3C MiniApps

In 2019, a dozen major global companies, including Alibaba, Baidu, Huawei, Intel, and Xiaomi, launched the W3C [MiniApps _Ecosystem Community_ Group](https://www.w3.org/community/miniapps/), a working group to incubate homogeneous specifications to define a common format for the light mobile applications widely spreading in Asia. Later that year, this W3C group released the [MiniApp Standardization White Paper](https://www.w3.org/TR/mini-app-white-paper/) as the first attempt to collect the requirements and use cases for subsequent standardization work. The white paper analyzes many of the current **MiniApp implementations** like Alipay Mini Program, Baidu Smart Mini Program, Quick Apps, and 360 PC MiniApp.

This successful incubation work motivated the establishment of a formal standardization group, [W3C MiniApps _Working_ Group](https://www.w3.org/2021/miniapps/), in 2021. The group, led by the biggest players in this nascent MiniApps ecosystem, Alibaba, Baidu, and Huawei, is dedicated to the in-depth exploration and coordination of the diverse MiniApp ecosystem with relevant W3C members like Google, Microsoft, Rakuten, and the public. The resulting technical specifications produced by this group aim to:

- Promote a common set of APIs, data formats, and capabilities to enhance **interoperability** between different MiniApp platform implementations from different vendors, such that a " **develop once, run anywhere**" paradigm enabled for the benefit of application users, developers, and publishers.
- Maximize the **integration** of MiniApps and **the Web**, thereby reducing technological fragmentation and developer learning costs.

Quick Apps is expected to be one of the first operational implementations of the emerging set of MiniApps standards.

The working group plans to release three major MiniApps standards by the end of 2022: _MiniApp Manifest_, a JSON-based manifest document that enables developers to set up metadata about the application (i.e., descriptive information, window styling, page routing, and feature policies); _MiniApp Packaging_, describing the package structure, and the components and resources (i.e., page templates, components, stylesheets, scripts, internationalization information, security resources, and the manifest); and _MiniApp Lifecycle_, a document that specifies the lifecycle events and the process to manage the lifecycle events of apps and individual pages.

The [W3C MiniApps Working Group](https://www.w3.org/2021/miniapps/) and the [W3C MiniApps Ecosystem Community Group](https://www.w3.org/community/miniapps/) continue their standardization and research works, inviting all MiniApps vendors and developers to participate.

> "_We need more sustainable and ethical apps with a direct and positive impact on the environment and society." - Adrien Henni, Executive President at Alliance Tech._

### Quick App Alliance

In 2018, ten major Chinese companies, including Huawei, OPPO, Vivo, and Xiaomi, created the [Quick App Alliance](https://www.quickapp.cn/) as an industrial forum to develop Quick Apps as an open technology, supporting device makers and developers interested in spreading the use of Quick Apps.

The [Quick App Alliance](https://www.quickapp.cn/) has developed a community in China with thousands of developers interacting with the Quick App vendors through different activities like developer events, forums, and tools offered through the official website.

This alliance organizes collaboration activities with external partners, helping all to develop services and create shared opportunities.

One example of a successful collaboration happened in June 2020, when Vipshop (the brand behind the famous Chinese retailer vip.com) launched the _Quick App Mid-Year Sale Festival_, a seven-day deal event in collaboration with all [Quick App Alliance](https://www.quickapp.cn/) members. A dedicated version of the Vip.com Quick App, with a joint marketing campaign, resulted in **5.365 million users** visiting the e-commerce site via the Quick App that generated **20,000 new customers**.

![Vipshop campaign exposure opportunities](/img/whitepaper/fig12.png)
_Figure 12. Vipshop campaign exposure opportunities_

## European digital strategy alignment

The European Commission has set out a digital strategy that defines the future of digital services within the EU that cover citizens' daily activities, online markets, and other aspects such as environment and sustainability. This digital strategy (enshrined most recently within the [Digital Services Act](https://ec.europa.eu/info/strategy/priorities-2019-2024/europe-fit-digital-age/digital-services-act-ensuring-safe-and-accountable-online-environment_en) and [Digital Markets Act](https://ec.europa.eu/info/strategy/priorities-2019-2024/europe-fit-digital-age/digital-markets-act-ensuring-fair-and-open-digital-markets_en)) includes up-to-date regulations involving e-Privacy, Data Governance, Web Accessibility, Digital Markets, and the Digital Services arising from applications, operating systems, platforms, online services, and app marketplaces. These regulations aim to **protect consumers**, reduce social, digital exclusion, and **increase choice** whilst improving the **efficiency** and **competitiveness** of the digital industry and foster **innovation and business fairness**.

The Quick Apps platform promotes these principles, offering diversity in advertising, fairness and competitive practices in marketplaces, and integration of third-party services. Quick Apps provide new mechanisms to enhance **accessibility and usability**, bringing new services to people with physical and cognitive restrictions or limited interaction with traditional applications. Simplification of access to a light app, customizable interfaces, and usage of advanced system capabilities (brightness control, audio volume, speech recognition…) make Quick Apps and widgets a perfect solution to introduce new services into the growing **silver market** (i.e., remote health, education, emergencies, and infotainment).

Complementing EU policies, the European Commission coordinates different funding programs to facilitate collaboration and mitigate the impact of research and innovation in developing and supporting these regulatory actions. The spread of knowledge and uptake of cutting-edge technologies are critical motivations for these funding calls. These objectives, plus **open data** and **open science** principles, are natural complements of Quick Apps as an implementation of **open standards that foster innovation**. Through the OW2 Quick App Initiative, a commons will be collaboratively developed and diffused to promote these principles.

# Case Studies

## Case study: Games

The gaming industry is a multi-billion dollar global market that attracts a wide range of profiles. Casual mobile gaming entertains all kinds of audiences and also brings exciting business opportunities. Of course, no success is guaranteed, but this growing market increasingly attracts new developers and publishers seeking to start profitable ventures by delivering instant playable entertainment.

Traditionally, gaming companies have faced limiting factors when developing casual light games, such as **device computing power**, **device connection speed**, and **structural market support**. The first two points are somewhat self-explanatory. If the average user's device is not fast enough or is not connected well enough to stream and play game content reliably, the company will fail.

Nowadays, the primary constraint is structural market support, defined as the ecosystem of tools and options for both users and businesses. It includes aspects like what do industry players have access to, what tools developers can use to create light games, how they can monetize their game's experience, what platform features are made available with respect to user insight and retention, etc.

![Gold Digger](/img/whitepaper/fig13.png)
_Figure 13. Gold Digger_

The light games industry is still finding its feet, but it won't be long before it's sprinting. In previous years, capabilities like in-app purchase support, adding home-screen icons, and standardized sharing and notification structures were the exceptions. These features are now either live (as in Quick Apps) or on the development roadmaps of most of the world's largest digital ecosystems. This new situation is shifting where and how the games industry can meet its users, and it promises some fascinating years ahead.

### Why Quick Games?

FRVR, an international gaming company with experience in casual games, set out to test and measure the value of the Quick App ecosystem against the conventional mobile app marketplaces with _Gold Digger FRVR_, a match-three mining game with base progression elements.

FRVR publishes the same games on many different platforms, including Facebook, Google Play, Huawei, Samsung, Snapchat, WeChat, the Apple App Store, and over 30 smaller venues. This experience qualifies FRVR as an expert to evaluate the relative value of _a platform to a game_ instead of just the value of _a game to a platform_; it has allowed FRVR to really **evaluate whether it's worth publishing Quick Apps or not**.

#### Reason #1: User value parity

The first experiment analyses the value of new users in Quick Apps. For this, FRVR considered the hypothesis that **traditional app-level** _ **user lifetime value** _ -the total economic value of a user over the time they play a game- **can also be derived from Quick App users**.

If we suppose that this hypothesis is true, Quick Apps might be considered a multi-billion dollar landscape. Setting aside the fact that Quick Apps have neither the download nor app marketplace reliance limitations of traditional mobile apps, we consider that there is no functional difference between how a Quick App and a mobile app can monetize. So, the primary index of user lifetime value appropriate for comparing the two ecosystems is _user retention_. If there is no difference between user retention on native apps vs. Quick Apps, users are equally engaged and similarly monetized.

Analyzing the user retention rate for both types of applications, it can be seen in Figure 14 that Quick App users have significantly worse retention rates than native app users. So does this disprove the hypothesis?

![Gold Digger FRVR user retention for Huawei Quick App vs. traditional app](/img/whitepaper/fig14.png)
_Figure 14. Gold Digger FRVR user retention for Huawei Quick App vs. traditional app_

In fact, these differences are to be expected because:

- 70% of users will not make it past the download stage for a traditional app, so users who do so are by definition more highly invested and engaged than Quick App users who do not have this hurdle; and
- By default, Quick App users do not have common native app retention mechanisms (e.g., home-screen icons).

As shown in Figure 15, a Quick App with app retention features active leads to a user retention profile that is similar, if not better, than the native app version. [^2]

[^2]: Note that the discrepancy in the shape of the retention curve is due to low-investment users of the traditional-app version never even installing the game.

Although there is a sampling bias in this comparison, we can justifiably ask the following question: **Is it easier to get a Quick App user to install mobile features** (like a home-screen icon) **than getting a non-user to download the native app?**

FRVR is highly confident that with good quality games that actively design for these feature conversions as part of the flow, this is an easy target to beat.

![Gold Digger FRVR user retention for Huawei Quick Apps with home-screen icons (HIS) vs. traditional app](/img/whitepaper/fig15.png)
_Figure 15. Gold Digger FRVR user retention for Huawei Quick Apps with home-screen icons (HIS) vs. traditional app_

#### Reason #2: Revenue scale

The second experiment hypothesizes that **$1m/month is a feasible expectation for a Quick Game**. So, if a native app-level experience can be delivered through Quick Apps, we can see the native app-level value… but is the scale there yet?

To investigate this, FRVR modeled the scenario given for an initial (D0) home-screen-icon conversion rate of just 20% and calculated the required monthly new users needed to make _Gold Digger FRVR_ a $1m/month title on the Quick App platform. Figure 16 illustrates the results of the model based on the following input criteria:

- Deployment on the Huawei Quick App platform and using its advertising eCPM (effective cost per thousand impressions) for tier-1 European countries;
- An iAP revenue split percentage consistent with _Gold Digger FRVR_'s performance across dozens of distribution channels;
- Using retention models based on FRVR industry-wide data, with one-year projections (D365), underestimating values past day 56 (D56);
- Actual _Gold Digger FRVR_ Quick App daily ad view figures;
- A _postulated_ new user number of 2.6 million per month, in line with the observed industry trends.

By taking existing actual retention and monetization data and projecting purely on new user growth, the research shows the level of revenue that a Quick Game stands to make on the Quick App platform, confirming the $1m/month expectations.

![Gold Digger FRVR Quick App revenue projections](/img/whitepaper/fig16.png)
_Figure 16. Gold Digger FRVR Quick App revenue projections_

The last puzzle piece is _user acquisition_. FRVR is currently working to validate the systems required to attract users directly into Quick Apps to guarantee the projected hyper-growth for quality game developers on the platform.

### A milestone, not a destination

While this case study is essentially a comparison between Quick Apps on the Huawei Quick App platform against native mobile apps, it is by no means the end state for light apps.

The future of game distribution is open, and the winners will be the companies that collaborate and support interoperability and innovation. It allows companies like FRVR to work together and turn the concept of distribution inside out. Instead of forcing users into a single store and trying to own the relationship, the games meet the players wherever they already are. Companies no longer try to control the consumer relationship but rather add value to it and collaborate with others that do the same.

Today, Quick Apps are giving never before seen access to the $180b games market. For many, this will be a **once-in-a-lifetime opportunity**.

> "_For more than a decade, billions of smartphone users have discovered, installed and paid for software under the sole control of the app marketplaces and stores. That is rapidly coming to an end, and the market opportunity is enormous." - Brian Meidell, FRVR CEO_

## Case study: Accommodation

The explosion of connected objects (often called the Internet of Things –or simply IoT) in our daily lives has introduced new opportunities to increase efficiency and enhance the quality of life. Intelligent systems in buildings allow us to interact with our "constructed" environment and access a wide variety of services, from security (CCTV, access control, air quality sensors, alarms…) to comfort control (temperature, lighting, airflow control…). Such intelligent systems also increasingly support integration with other home appliances (Smart TVs, smart agents, and refrigerators).

These services convert buildings into an extension of a user's smart devices, enabling direct communication with the home or public facilities. **Accessing building services through the device** allows users to maximize user experience, customize appearance and interaction according to their preferences and accessibility requirements.

### Generic components for sensors and appliances

![Hotel room sensors, actuators, and services](/img/whitepaper/fig17.png)
_Figure 17. Hotel room sensors, actuators, and services_

The [W3C Web of Things (WoT)](https://www.w3.org/WoT/) standard enables easy integration across IoT platforms and application domains. These WoT technologies, applied to sensors and actuators in buildings, define a high-level layer to identify, describe and interact with light switches, temperature thermostats, door locks, and any other web-enabled device. WoT brings developers the possibility to create applications that understand the nature of sensors and devices and deliver apps that define homogeneous and intuitive user interfaces – a user interacts with the hotel room thermostat in the same way as they do at home, independently of the electronics vendor.

Quick App platform is a good partner for WoT, implementing **generic components for WoT** that allow **automatic discovery** and use of sensors and actuators. These WoT components may define standard user interfaces for common appliances and sensors like switches, temperature sensors, temperature regulators, and alarms.

The customization flexibility in the Quick App components affects usability and accessibility, creating subsequent opportunities in specific markets such as in the accommodation industry.
  
### Universal user experience

Hotel chains that implement WoT technology may offer guests tailor-made experiences through Quick Apps, allowing guests to interact with their devices. The user experience begins at the check-in stage when the hotel system generates a new Quick App with exclusive encryption keys, based on the receptionist's request and guest's profile and preferences.

The Quick App, associated with the guest's room, offers information and services to the customer in their preferred language.

Once the Quick App is generated, the customer receives a text with a link to use it. This same link will also be displayed as a QR code on the TV set within the room. The app provides customers with information about room comfort (temperature, fire alarm, air quality, and door lock status). It also allows them to interact with the light switches and the coffee machine and manage the air conditioning and heating system.

The user interface, configured with guest's **language** and **cultural preferences** (i.e., measurement units, currency, symbols, and colors), is complemented with **homogeneous and intuitive controls**. Thus, Quick Apps become a powerful tool for assisting all kinds of public, regardless of cultural differences and disabilities (i.e., visual imparities, reduced movements, cognitive aspects, and other limitations). Every app includes specific configurations and privilege control for each facility and authorized guest.

### More interaction, better guest experience

The app's configuration may include specific accessibility aspects to help users find helpful information about the hotel facilities or guest's security and interact with the hotel services like concierge and reception requests, room service orders, restaurant bookings, and other personal assistance requests.

![Hotel room controls, push notifications](/img/whitepaper/fig19.png)
_Figure 18. Hotel room controls and push notifications_

On the other hand, the hotel may use the Quick App to provide guests with **real-time information**, including event schedules, direct alerts (e.g., earthquake and fire alarms), **external service** reminders (e.g., flight delays and taxi arrivals), and **direct marketing** activities (e.g., vouchers and restaurant discounts).

Complementary to notifications from the hotel, they may offer on-demand services. Thus guests may **book and purchase services and products** through the same application and using one-click payment mechanisms using the device's security (e.g., sports services and material, late checkout, ironing, additional amenities, press, and TV).

--- 


# Acknowledgments and feedback

This white paper was written with the support of the Quick App Initiative and its participants. The authors are especially grateful to the contributors **Martin O'Brien**, **Michele Pastore**, **Vivian Lin** and **Yongjing Zhang**, as well as the following companies for sharing their resources: [_Alliance Tech_](https://alliance-tech.eu/), [_CTIC/W3C Spain Chapter Hub_](https://ctic.es/), [_Famobi_](https://www.famobi.com/), [_Flecha Amarilla_](https://wl.primeraplus.com.mx/), [_FRVR_](https://frvr.com/), [_Huawei_](https://www.huawei.com/), [_MapMyIndia_](https://www.mapmyindia.com/), _RSI Foundation_, [_Santillana Global S.L._](https://santillana.com/en/), [_TravelGo_](https://www.travelgo.com/), and [_Vipshop_](https://www.vip.com/). 

📸: _Albert Hu on Unsplash_, _HalGatewood.com on Unsplash_, _Paul Postema on Unsplash_

Feel free to send feedback to our mailing list [quickapp@ow2.org](mailto:quickapp@ow2.org) or [raise an issue](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/issues/new) in the Quick App Initiative repository.

**June 2021. OW2 Quick App Initiative.**
