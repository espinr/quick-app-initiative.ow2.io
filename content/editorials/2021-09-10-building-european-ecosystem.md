---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: false
title: Beyond addressable market
subtitle: Building a quick app ecosystem in Europe 
date: 2021-08-26
tags: ["OpEd"]
author: Christian Paterson
bigimg: [{src: "/img/posts/2021/427052_ESA_First_Sun_201908.jpg", desc: "First Sun; Credits: ESA/IPEV/PNRA–N. Albtersen"}]
comments: true
---


Searching app stores and downloading mobile apps is a barrier to what we want to do *now*. Quick apps offer an alternative to the traditional mobile app dynamic by enabling an *in-the-moment* experience that delivers *immediacy of value*. One of the core challenges to developing a quick app ecosystem in Europe is achieving quick app support on as many devices as possible. Yet despite this challenge, there are multiple reasons why European businesses and organisations should be interested in nuturing this ecosystem *today*. The OW2 Quick App Initiative, is a neutral, open source forum to *collaboratively* foster this opportunity *in Europe, for Europe*.

<!--more-->


### Spot the problem

Searching online app stores and downloading mobile apps is so yesterday. It takes time, is a barrier to what we want to do *now*, fills device storage, invites unwanted notifications, burdens our phones with apps we only needed momentarily, sucks up network bandwidth and mental energy. It’s just so 2008

> Searching app stores and downloading mobile apps, takes time and is a barrier to what we want to do *now*.

Yet, as a society we demand an increasingly connected, app-driven world, with our ever-at-the-ready mobile devices providing amazing little gateways to online nirvana. And in so doing, we accept the negative experiences mentioned above which we believe are the price to pay.

> We accept the negative experiences as the price to pay for obtaining online nirvana in an increasingly connected, app-driven world.

Conjunctionally to **app fatigue**[1][2], developers struggle to surface their apps above the waterline, and many businesses (especially SMEs) find dipping their toes in the app swimming pool daunting, and so miss the powerful customer experience, engagement and revenue that mobile apps can provide. The growth of mobile apps -in their current incarnation- for quite literally everything, is unintuitively leading us away from the promised land.


### Future fit

Quick apps offer an alternative to the traditional mobile app dynamic by enabling a truly **in-the-moment** experience that foregoes the burden of search and download for an **immediacy of value**. They deliver a rich and powerful app experience, yet are launched instantly by scanning QR codes, selecting “deep” links from within apps, or clicking weblinks in emails and SMS (only from trusted sources of course).

> Quick apps offer an alternative to the traditional mobile app dynamic by enabling an *in-the-moment* experience that delivers *immediacy of value*.

Furthermore, quick apps are faster and lighter to develop than their equivalent native apps, require less bandwidth to acquire, and do not need to be published through a traditional app store. They offer a **democratisation of the mobile app** for business and developers, at the same time as empowering a more immediate experience for users.

To help foster this nascent technology, the **OW2 Quick App Initiative**[3] (QAI) has recently been launched in Europe by several companies and organisations. It is founded on the **principles of open source** (collaboration, transparency, meritocracy, sharing) in the belief that *diversity engenders progress*.

A key question is of course the deceptively simple one; *why* should any given organisation be interested in quick apps and the QAI?


### Why invest the time and energy?

Whilst different organisations will come at this from different perspectives, **addressable market** (audience reach) will surely be a common criterion … and I suspect, one that is pretty high on the priority list. This means that one of the core challenges to developing the ecosystem will be achieving quick app support on as many devices from as many vendors as possible. In Europe, to my knowledge only Huawei smartphones are “quick app ready” out of the box. In China, other companies like Xiaomi, OPPO, Vivo and Lenovo also support quick apps.

We can draw 2 points from this:

1. The addressable market for quick apps in Europe is "relatively" limited.
    - In 2020, Huawei *only* shipped about 13% of the European smartphone market[4], but then again, that still represents 23M smartphones[5]. A big number by anyone’s reckoning ... and, this doesn’t count the millions of smartphones Huawei shipped prior to 2020.
2. The addressable market in China is significantly larger given the number of supporting vendors (although I haven’t looked for stats).


### European companies *should*  be interested

Considering *just* the addressable market today, we can identify 2 **types of company** that should have an interest in seeing a quick app ecosystem develop in Europe:

- **Type 1:** Those interested in leveraging their quick apps not just in Europe, but also to access the huge Chinese market
   --> export opportunities.

- **Type 2:** Those interested in accessing the x million quick app supporting devices in Europe today. Including offering services and experiences to visiting Chinese people
   --> local opportunities.
   
Beyond addressable market, are there **other considerations** that might attract companies, non-profits, academia and even public bodies?

- **Type 3:** Those interested in creating and publishing *new* services on *alternative* platforms
   --> innovation.

- **Type 4:** Those interested in creating a **new app dynamic** ready for the "connected everything". First to market doesn’t always guarantee success (Betamax vs. VHS) but it certainly helps (Coca-Cola, Netflix, Amazon, eBay …)
   --> future fit.

- **Type 5:** Those motivated to see the development of an app dynamic that encourages **greater app distribution freedoms** … or put another way, a reduction of marketplace lock-in/lock-out. As an aside, this aligns well the with digital sovereignty zeitgeist, as well as various high-profile app store anti-trust battles
   --> digital sovereignty.


### Paradigm shift

The *promise* of quick apps to transform how end-users acquire and run apps, as well as how businesses produce and distribute apps, is *potentially* game changing.

Organisations that fall into the above categories **should be very interested in the Quick App Initiative**:

- An open forum of knowledge building and experience sharing
 --> Create awareness and best practices within and across verticals.
 
- A neutral place to co-develop tools, code templates, examples and documentation
 --> Facilitate the developer experience.
 
- A rich source of input into the related W3C standardization process
 --> Ensure that different domain and industry needs are represented.
 
- A collaboration space to create open source “reference implementations” of quick app engines
 --> Reduce barriers for device vendors to provide quick app support.

The OW2 Quick App Initiative is in its infancy, just like the nascent quick app ecosystem. Many challenges lie ahead, but the potential to grow a new, more democratised and fluid mobile app dynamic that step us beyond 2008 is something worth striving for.

The QAI warmly welcomes all who are motivated by the potential and the challenge, without prejudice and without membership cost imposition. We look forwards to seeing you.

---

Please visit the **[web portal](https://quick-app-initiative.ow2.io/)**, or sign-up to the **[mailing list](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info)**

---

    [1] https://clevertap.com/blog/app-fatigue/
    [2] https://techcrunch.com/2016/02/03/app-fatigue/
    [3] https://www.ow2.org/view/QuickApp/
    [4] https://www.statista.com/statistics/1227302/smartphone market-share-in-europe/
    [5] https://www.counterpointresearch.com/european-smartphone-market-2020/
