---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: New Developer Section
subtitle: Learn how to code your quick apps today
date: 2021-12-10
tags: [developers,docs]
author: Martin Alvarez
bigimg: [{src: "/img/posts/2021/coding.png", desc: "Coding (Sora Shimazaki, Pexels)"}]
comments: true
draft: true
---

Are you interested in developing quick apps? In the new [section for developers](/developers/), now you can find comprehensive documents and guides that will help you __understand quick apps__ and __create your first app__.

<!--more-->

Among the documents, you can find a [getting started guide](/developers/guide/getting-started.html) explaining the requirements and tools to kick start the development and test your first apps in minutes. There, you can also learn about [the structure of the `rpk` packages](/developers/guide/directory.html) that form quick apps, how to configure your application's look and feel with [the manifest](/developers/guide/manifest.html), and start writing your first [.ux documents](/developers/guide/ux-documents.html) with a structure and content based on HTML-like language, binding events and applying stylesheets and advanced business logic based on rich services.

You can also find reference documents with technical specifications of the [user interface components](/developers/guide/components-basics.html) (i.e., containers, images, tabs, among others) and the [services and APIs](/developers/guide/api-basics.html) used for the application's business logic.

The following table summarizes the documents you will find in this new section:

| Document | What is it about? |
| -------- | ----------------- |
| **Quick Guide** |  | 
| [Introduction](https://quick-app-initiative.ow2.io/developers/guide/) | Basic concepts |
| [Getting Started](https://quick-app-initiative.ow2.io/developers/guide/getting-started.html) | Prerequisites, environment and tools, compile and run the app |
| [Directory Structure](https://quick-app-initiative.ow2.io/developers/guide/directory.html) | Structure of a `.rpk` file |
| [Manifest](https://quick-app-initiative.ow2.io/developers/guide/manifest.html) | Main configuration file structure and content  |
| [UX Documents](https://quick-app-initiative.ow2.io/developers/guide/ux-documents.html) | Structure of the files to implement quick apps |
| [Template Syntax](https://quick-app-initiative.ow2.io/developers/guide/templates.html) | Syntax of the UX documents |
| [App Styling](https://quick-app-initiative.ow2.io/developers/guide/styling.html) | Stylesheet details and syntax |
| [Scripting](https://quick-app-initiative.ow2.io/developers/guide/scripting.html) | How to describe scripts |
| [Event Handling](https://quick-app-initiative.ow2.io/developers/guide/events.html) | Event definition and management |
| [App/Page Lifecycle](https://quick-app-initiative.ow2.io/developers/guide/lifecycle.html) | Lifecycle events in quick apps |
| [Internationalization](https://quick-app-initiative.ow2.io/developers/guide/i18n.html) | Mechanisms to localize apps |
| [Components Basics](https://quick-app-initiative.ow2.io/developers/guide/components-basics.html) | Introduction to UI components  |
| [APIs and Services Basics](https://quick-app-initiative.ow2.io/developers/guide/api-basics.html) | Introduction to APIs and services |
| [Services Running in Background](https://quick-app-initiative.ow2.io/developers/guide/background.html) | How to run services in the background |
| [Distribution](https://quick-app-initiative.ow2.io/developers/guide/distribution.html) | In case you want to list it on a marketplace |
| **UI Components**  | |
| [Elements Summary](https://quick-app-initiative.ow2.io/developers/components/) | ~30 Pre-built components |
| [Common Attributes](https://quick-app-initiative.ow2.io/developers/components/attributes.html) | Element's attributes |
| [Common Styles](https://quick-app-initiative.ow2.io/developers/components/styles.html) | CSS properties and rules |
| [Common Events](https://quick-app-initiative.ow2.io/developers/components/events.html) | Element's events |
| **API & Services**  | |
| [Summary of APIs](https://quick-app-initiative.ow2.io/developers/services/) | ~50 Services and APIs to use in quick apps  |

**Read more** about the technical aspects of quick apps on the [developer section](/developers/) and [join the Quick App Initiative](https://www.ow2.org/view/QuickApp/Participants_Form) today to get involved and enhance the content.
