---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: Easy coding with quick apps
subtitle: Prototyping with quick apps 
author: Martin Alvarez
date: 2021-10-26
tags: ["code"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
comments: true
---

In September this year, I attended [AthTech'21](https://athtech.run/2021), a conference on sports data and technology. There, we discussed systems interoperability and user (fans) engagement through technology. During one of the sessions, I [presented an idea](https://espinr.github.io/talks/2021/0923-QuickApp-LJ/) to attract the spectators' interest in concrete athletics events like throws and jumps. I developed a simple but functional prototype on a quick app.     

<!--more-->

The [result](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/tree/master/use-cases/athletics-longjump) is a quick app with information about competitor's performances in a competition, notifications. and sharing using third-party apps:

{{< rawhtml >}}
<img style="height: 500px; border-radius: 6px; border: 6px solid rgb(51, 51, 51);" alt="Long Jump quick app" src="/img/posts/2021/longjump.gif"/>
{{< /rawhtml >}}

This framework enables us to experiment with users interfaces, quick prototyping, and advanced capabilities to make the most of users' devices. Its modular approach allows us to reuse UI components and access dedicated APIs and services to facilitate the development of simple and light applications with excellent user experience. This article explains the key aspects of the quick app's development, applying to a real example that you could reuse and try by yourself.


## Pages, Components and Elements

Quick apps are composed of reusable components organized in pages and defined in documents identified by the extension `.ux`, encapsulating the quick app pages structure, logic, and rendering information.

We can reuse the components, scoping the functionality and user interfaces for a better organization and maintenance of the quick app. As shown in the following picture, we can define quick app components based on [essential elements](https://quick-app-initiative.ow2.io/developers/components/) like in HTML (i.e., [lists](https://quick-app-initiative.ow2.io/developers/components/list), [images](https://quick-app-initiative.ow2.io/developers/components/image), [divs](https://quick-app-initiative.ow2.io/developers/components/div), ...) and other components in a hierarchical way.

![Quick App Components](/img/posts/2021/quickappcomponents.svg)

### Build a Component  

We can either define our local components or use external ones, like [this open source library](https://github.com/espinr/quick-app-ui). 

Quick app components are defined in [UX documents](https://quick-app-initiative.ow2.io/developers/guide/ux-documents.html) structured in three sections:

- __Rendering template__. Identified by the `<template>` tag, this section specifies the structure of the component, including rendering information (i.e., content and visual elements) and logic (i.e., conditionals, loops,..). 
- __Stylesheet__. Identified by the `<style>` tag, this optional section includes the local stylesheets of the current component.
- __Scripts__. Identified by the `<script>` tag, this section defines the components' functions and business logic, including managing the lifecycle of the instances created in runtime (i.e., events when created, destroyed, etc.). 

[Templates](https://quick-app-initiative.ow2.io/developers/guide/templates.html) support data interpolation to bind data variables and functions from the script to the view, enabling complex operations on the logic engine. The separation of the logic (JavaScript) and rendering engines brings us reactivity and high performance. We can also bind user interaction events produced in the view with handlers in the component's logic.

We can synchronize data between the logic and the view using the _{{mustache}}_ notation and binding variables.

The following example shows [a component](https://quick-app-initiative.ow2.io/developers/guide/components-basics.html) with an image and a text. The structure of the component is built with the [basic elements](https://quick-app-initiative.ow2.io/developers/components), `div`, `image`, and `text`. The content is specified in the `data` object within the `script`. Any change on these variables will be rendered reactively.    

``` html
<template>
  <div class="header">
      <div class="header_box">
        <div>
          <image src="{{ logoUrl }}"></image>
          <text>{{ titleContent.title }}</text>
        </div>
      </div>
  </div>
</template> 
<style>
  .header {
    flex-direction: column;
    justify-content: space-between;
    width: 100%;
    margin-bottom: 8px;
  }
  .header_box {
    height: 36px;
    width: 230px;
    margin-left: 12px;
  }
</style>
<script>
  module.exports = {
    props: ["titleContent"],
    data: {
      titleContent: {
        title: "Title of the quick app"
      },
      logoUrl: "https://example.org/images/logo.png",
    },
    onInit: function () {
        console.log('onInit triggered')
    },
  };
</script>
```

In the `style` section, we can apply [styles to the components](https://quick-app-initiative.ow2.io/developers/guide/styling.html). We can use a profile of the standard CSS but also [preprocessing with LESS and Saas](https://quick-app-initiative.ow2.io/developers/guide/styling.html#css-preprocessing).

### Reuse Components 

As shown in [the picture above](#pages-components-and-elements),we can create several sub-components as part of a component to simplify the app's maintenance and for a cleaner code. For instance, we want our quick app's pages to always have the same structure, so we create three sub-components:  `header`, `grid` and `footer`. We define the components individually and load them through the `import` directive. Once they are loaded, we can directly use them as another essential element within the component. 

For instance, we can import the component `header.ux` and use it within the main component, passing parameters from the parent as the header's title.

``` html
<import name="header" src="./common/component/header/header.ux"></import>
<import name="footer" src="./common/component/footer/footer.ux"></import>
<import name="grid" src="./common/component/grid/grid.ux"></import>
<template>
  <div class="grid_box" >
    <header title-content="{{ titleContent }}"></header>
    <grid></grid>
    <footer></footer>
  </div>
</template> 
<script>
  module.exports = {
    data: {
      titleContent: "My app"
    }
  }
</script>
```

With this modular hierarchical approach, we can create structures as complex as we need. Now, we need to define the application's routes (the pages or sections) and the components associated with each page. If we want to create two pages, _home_ and _detail_, whose components are defined in components with the same name (`home.ux` and `detail.ux`), we need to specify this in the `manifest.json` document:


``` js
{
  // ...
  "router": {
    "entry": "home",            // The entry page is "home"
    "pages": {
      "home": {
        "component": "home",    // "home.ux" (no need to specify the extension)
        "path": "/"             // Component located at {src}/home.ux
      },
      "detail": {
        "component": "detail",  // "detail.ux" (no need to specify the extension)
        "path": "/detail"       // Component located at {src}/detail/detail.ux
      }      
    }
  },
  // ...
}
```  

In the [manifest](https://quick-app-initiative.ow2.io/developers/guide/manifest.html), we can also configure the general look and feel of the application (i.e., orientation, colors, menus, ...), other setup options, and basic metadata about the application. Read more about the [quick app manifest](https://quick-app-initiative.ow2.io/developers/guide/manifest.html). 

## Use APIs and Services

Apart from the rendering part based on components, we can use [built-in APIs and services](https://quick-app-initiative.ow2.io/developers/services/) to deliver rich user experiences, from [push notifications](https://quick-app-initiative.ow2.io/developers/services/notification.html) and [user dialogs](https://quick-app-initiative.ow2.io/developers/services/pop-up.html) to advanced multimedia management and efficient control of the communications (e.g., fetch resources, upload/downloads,...).

In case we need to use one of these [services](https://quick-app-initiative.ow2.io/developers/services/), you usually need to specify it in two places:

__Step 1__: Add the service in the `manifest.json`, as an advanced feature:

``` js
{
  // ...
  "features": [
    {
      "name": "system.prompt"           // Service to show dialogs
    },
    { 
      "name": "system.notification"     // Service for push notifications
    },
    {
      "name": "system.share"            // For third-party data sharing
    }
  ],
  // ...
}
``` 

These declarations enable the system to pack only the libraries the app needs in compilation time. 

__Step 2__: Import these services and use them in the scripts:


``` html
<template>
    <div>
        <div class="item" onclick="shareResult">
            <text class="item_attempt">Click me to share something</text>
        </div>
    </div>
</template>
<script>
    import share from '@system.share';
    module.exports = {
        shareResult() {
            share.share({
                type:"text/html",
                data:"Hey, I'm sharing data with others!!",
                success: function(data) {
                    console.log("Success sharing");
                },
                fail: function(data, code) {
                    console.log("Sharing failed, code=" + code);
                }
            });
        }
    };
</script>
```

Following this way, we can apply more advanced features, like running services [in the background](https://quick-app-initiative.ow2.io/developers/guide/background.html), checking and installing native applications in the device, accessing the calendar and contacts. See [all services](https://quick-app-initiative.ow2.io/developers/services/).

You can [get the code from the Quick App Initiative's repository](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/tree/master/use-cases/athletics-longjump) and enhance the app with real athletes and data. If you are interested in learning more about the quick app framework, look at our [section for developers](https://quick-app-initiative.ow2.io/developers/), with a quick start guide and reference documents about the quick app platform. You are also invited to [join us](https://quick-app-initiative.ow2.io/page/about/#you-are-welcome-to-participate) and contribute with more examples, guides, and opinions.  