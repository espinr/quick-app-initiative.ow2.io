---
type: post
# Do not remove 'type: post'
highlight: false
# highlight = true to show the excerpt on the homepage 
title: OW2 Establishes Quick App Initiative
subtitle: Announced at OW2Con'21 
date: 2021-06-23
tags: ["qai"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
comments: true
---

23 Jun 2021 — The [Quick App Initiative](/) was formally announced at [OW2Con'21](https://www.ow2con.org/view/2021/). This interest group will gather global experts from a wide range of industries and fields to explore new applications in various vertical fields for quick apps, while also fostering technological innovation, entrepreneurship, and ecosystem.

<!--more-->

During his keynote at OW2Con'21, **Mr. Xuemin Wang**, Vice President of the Huawei European Research Institute, noted that the OW2 Quick App Initiative was created in support of the [W3C MiniApp model](https://w3c.github.io/miniapp/white-paper/) (represented by [quick apps](/page/whitepaper/#what-is-a-quick-app)) to foster industry consensus, to raise awareness of this empowering dynamic, and to engage the global developer community. Together, initiative participants will expand this emerging ecosystem to create and deliver innovative applications across the many industries of a new digital era. 

![Workflow of the Quick App Initiative](/img/posts/2021/quickapp_initiative_workflow.jpg)

## MiniApp Model (Represented by Quick Apps) to Play a Major Role in New Era

Digitalization has inspired a whole host of new services that have revolutionized daily life. For example, hospitals have launched online registration and consultation platforms, airlines have opened up online booking and boarding; merchants offer promotions on e-commerce platforms; and mobile game companies have made their games easier than ever to access. [MiniApp](https://w3c.github.io/miniapp/white-paper/) is a new **web-based model for mobile app development** that lowers the “friction to use” dynamic: no installation, tap-to-use, and instant access. It enables users to easily and almost instantly access content and services whenever and wherever they need them.

![QR Code that opens a quick app for local transport](/img/posts/2021/quick-apps.png)

MiniApps are sure to play a critical role in the upcoming digital era. Quick apps, as an implementation of MiniApps, are remarkably easy for users to find and access, and address a broad range of mobile device usage scenarios, including those on Huawei devices. Users only need a single tap (or they can simply scan a QR code, or select a web link) to run quick apps. And, since quick apps are based on native rendering, just as native apps are, users can enjoy the same experience as with native apps, but without the hassle of the installation process. 

## Rapid Growth in Asia and Enormous Potential in Europe and Other Regions

The MiniApp model is quickly taking shape in Asia. Recent data shows that approximately the 10% of Internet traffic in China came from MiniApps in 2020. Currently, quick apps can be accessed on more than 1.2 billion online devices, and have a user retention rate over 70%. Compared with figures from last year, the number of active users of quick apps has increased by 37%, and the number of quick apps has increased by over 46%. Given their innovative user experience, quick apps have become a highly empowering and cost-effective choice in the 5G era.

In Europe, and many other regions, the MiniApp model has yet to be promoted on a major scale. Nonetheless, it offers enormous potential, allowing users in these regions to access games, go shopping, book airfare and hotels, take public transportation, and access public services, all without the trouble of installing an app. This saves both storage space and time, and enables a new “here and now” app access and usage dynamic, while enriching digital living immeasurably.

Read more information about this new paradigm in the [Quick App White Paper](/page/whitepaper/) already published by the Quick App Initiative.

Related press releases:
- [OW2](https://www.ow2.org/view/Press_Releases/OW2QuickApp "OW2 Press release in PDF format")
- [Huawei](/docs/Press_Release_-_Global_Open_Source_Community_OW2_Establishes_Quick_App_Initiative_to_Boost_Ecosystem.pdf "Huawei Press release in PDF format")
